﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TeachLearnWeb.Business
{
    public class Repository<T> : IRepository<T> where T : class
    {

        private DbContext _db;

        public Repository(DbContext _context)
        {
            _db = _context;
        }

        public DbContext db
        {
            get
            {
                return _db;
            }
            set
            {
                _db = value;
            }
        }

        public async Task AddAsync(T _item)
        {
            await Task.Factory.StartNew(() => db.Set<T>().Add(_item));
        }

        public async Task AttachAsync(T _item)
        {
            await Task.Factory.StartNew(() =>
            {
                db.Set<T>().Attach(_item);
            });
        }

        public async Task DeleteAsync(T _deletedItem)
        {
            await Task.Factory.StartNew(() => db.Set<T>().Remove(_deletedItem));
        }

        public async Task DeleteManyAsync(IEnumerable<T> _deletedList)
        {
            await Task.Factory.StartNew(() => db.Set<T>().RemoveRange(_deletedList));
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await db.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(int ID)
        {
            return await db.Set<T>().FindAsync(ID);
        }

        public async Task<IEnumerable<T>> GetByAsync(Func<T, bool> predicate)
        {
            return await Task.Factory.StartNew(() => db.Set<T>().Where(predicate));
        }

        public async Task<DbSet<T>> GetDbSetAsync()
        {
            return await Task.Factory.StartNew(() => db.Set<T>());
        }

        public async Task<T> SingleGetByAsync(Expression<Func<T, bool>> predicate)
        {
            //if (includes.Count()!=0)
            //{
            //    var query = db.Set<T>().Where(predicate);
            //    var result = includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
            //    return result.Cast<T>().SingleOrDefault();
            //}
            //else
            return await db.Set<T>().SingleOrDefaultAsync(predicate);

        }

        public async Task UpdateAsync(T _updatedItem)
        {
            await Task.Factory.StartNew(() =>
            {
                db.Set<T>().Attach(_updatedItem);
                db.Entry<T>(_updatedItem).State = EntityState.Modified;
            });
        }

    }
}
