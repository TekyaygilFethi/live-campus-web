﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Business
{
    interface IUnitOfWork : IDisposable
    {
        IRepository<T> GetRepository<T>() where T:class;
        Task<int> SaveChanges();





    }
}
