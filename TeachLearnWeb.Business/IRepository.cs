﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Business
{
    public interface IRepository<T> where T:class
    {
        Task AddAsync(T _item);
        Task UpdateAsync(T _newItem);
        Task DeleteAsync(T _deletedItem);
        Task DeleteManyAsync(IEnumerable<T> _deletedList);
        Task AttachAsync(T _item);
        Task<T> GetByIdAsync(int ID);
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetByAsync(Func<T, bool> predicate);
        Task<T> SingleGetByAsync(Expression<Func<T, bool>> predicate);
        Task<DbSet<T>> GetDbSetAsync();
        void Dispose();


    }
}
