﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.BusinessFolder
{
    interface IDbContextHelper
    {
        DbContext GetContext();
        void DisposeContext();

    }
}
