﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TeachLearnWeb.Data.DbContextFolder;

namespace TeachLearnWeb.BusinessFolder
{
    public class WebDbContextHelper : IDbContextHelper
    {
        private const string contextKey = "TeachLearnWebDbContext";
        
        public DbContext GetContext()
        {
            if (HttpContext.Current.Items[contextKey] == null)
            {
                HttpContext.Current.Items.Add(contextKey, new TeachLearnWebDbContext());
            }
            return (TeachLearnWebDbContext)HttpContext.Current.Items[contextKey];
        }

        public void DisposeContext()
        {
            if (HttpContext.Current.Items[contextKey] != null)
            {
                var context = (DbContext)HttpContext.Current.Items[contextKey];
                context.Dispose();
            }
        }
    }
}
