﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.DbContextFolder;

namespace TeachLearnWeb.BusinessFolder
{
    public class LogDbContextHelper : IDbContextHelper
    {
        private const string contextKey = "TeachLearnWebLogDbContext";

        public DbContext GetContext()
        {
            if (HttpContext.Current.Items[contextKey] == null)
            {
                HttpContext.Current.Items.Add(contextKey, new TeachLearnWebLogDbContext());
            }
            return (TeachLearnWebLogDbContext)HttpContext.Current.Items[contextKey];
        }


        public void DisposeContext()
        {
            if (HttpContext.Current.Items[contextKey] != null)
            {
                var context = (DbContext)HttpContext.Current.Items[contextKey];
                context.Dispose();
            }
        }
    }
}