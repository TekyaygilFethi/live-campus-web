﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TeachLearnWeb.ModelBinders
{
    public class AddLessonModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            string _lessonName = request.Form.Get("Name");
            string _CategoryID = request.Form.Get("Category");


            return new string[] { _lessonName, _CategoryID };
        }
    }
}