﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.Data.DbContextFolder;

namespace TeachLearnWeb.ModelBinders
{
    public class LoginFormData : IModelBinder
    {
        Repository<User> _userRepository = new Repository<User>(new TeachLearnWebDbContext());

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;


            var usernamemail = request.Form.Get("Username");
            var password = request.Form.Get("Password");

             Security.HashSaltClass.CheckCreedientals(usernamemail, password);



            return new object();



        }
    }
}