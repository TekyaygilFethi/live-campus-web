﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.Security;

namespace TeachLearnWeb.ModelBinders
{
    public class CreateAccountModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            var name = request.Form.Get("Name");
            var surname = request.Form.Get("Surname");
            var birthday = request.Form.Get("Birthday");
            var username = request.Form.Get("Username");
            var base64image = request.Form.Get("Base64Image");
            var email = request.Form.Get("Email");
            var password = request.Form.Get("Password");

            var salt = HashSaltClass.GenerateSalt();

            var _newPassword = HashSaltClass.GetHashSaltPassword(password,salt);
            
            User _user = new User();
            _user.Username = username;
            _user.Password = _newPassword;
            _user.Salt = salt;
            _user.Email = email;
            _user.ProfilePhoto = Convert.FromBase64String(base64image);

              Student _student = new Student();
            _student.Name = name;
            _student.Surname = surname;
            _student.Birthday = DateTime.Parse(birthday);

            _user.Student = _student;
            _student.User = _user;

            object[] dataArray = new object[2];
            dataArray[0] = _user;
            dataArray[1] = _student;

            return dataArray;

        }
    }
}