﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.ForumController
{
    public class CreateNewTopicFormData
    {
        public string TopicName { get; set; }

        public string Content { get; set; }

        public int ForumFacultyID { get; set; }

        public int ForumDepartmentID { get; set; }
        
    }
}