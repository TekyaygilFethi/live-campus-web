﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.ForumController
{
    public class TopicsPageFormData
    {
        public ForumFaculty ForumFaculty { get; set; }

        public ForumDepartment ForumDepartment { get; set; }
    }
}