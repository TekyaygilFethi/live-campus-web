﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.ForumController
{
    public class ForumIndexFormData
    {
        public IEnumerable<ForumFaculty> ForumFaculties { get; set; }

        public IEnumerable<ForumDepartment> ForumDepartments { get; set; }

    }
}