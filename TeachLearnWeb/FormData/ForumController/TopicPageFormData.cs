﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.ForumController
{
    public class TopicPageFormData
    {
        public Topic Topic { get; set; }

        public ICollection<UserProfilePhoto.ProfilePhotoFormData> ProfilePhotoFormDatas { get; set; }
    }
}