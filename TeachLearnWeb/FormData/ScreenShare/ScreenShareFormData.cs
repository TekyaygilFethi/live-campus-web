﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.ScreenShare
{
    public class ScreenShareFormData
    {
        public int SharerUser { get; set; }

        public User ViewerUser { get; set; }
    }
}