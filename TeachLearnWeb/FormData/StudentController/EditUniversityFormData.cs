﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.StudentController
{
    public class EditUniversityFormData
    {
        public int ID { get; set; } //educationID

        public string Year { get; set; }

        public string Semester { get; set; }

        public string University { get; set; }

        public string Faculty { get; set; }

        public string Department { get; set; }


    }
}