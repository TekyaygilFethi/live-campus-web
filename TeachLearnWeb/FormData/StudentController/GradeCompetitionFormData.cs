﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.StudentController
{
    public class GradeCompetitionFormData
    {
        public int Order { get; set; }

        public double TotalAverage { get; set; }

        public Student Student { get; set; }

        public string UniversityName { get; set; }

        public string FacultyName { get; set; }

        public string DepartmentName { get; set; }
    }
}