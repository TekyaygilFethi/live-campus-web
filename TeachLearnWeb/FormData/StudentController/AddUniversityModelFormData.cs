﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.Data.UniversityModel;

namespace TeachLearnWeb.FormData.StudentController
{
    public class AddUniversityModelFormData
    {
        public string SelectedUniversity { get; set; }

        public string SelectedFaculty { get; set; }

        public string SelectedDepartment { get; set; }

        public University University { get; set; }

        public Student Student { get; set; }
    }
}