﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.ConnectionController
{
    public class ConnectionDetailsFormData
    {
        public ConnectionDetailsFormData()
        {
            ProfilePhotoFormDatas = new List<UserProfilePhoto.ProfilePhotoFormData>();
        }
        public ICollection<UserProfilePhoto.ProfilePhotoFormData> ProfilePhotoFormDatas { get; set; }

        public Data.POCOs.TeachLearnWeb.Connection Connection { get; set; }
    }
}