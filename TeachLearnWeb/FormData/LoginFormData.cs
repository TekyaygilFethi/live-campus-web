﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData
{
    public class LoginFormData
    {
        [Index(IsUnique = true)]
        [MaxLength(30, ErrorMessage = "Username can not exceed 30 characters!")]
        public string Username { get; set; }

        public string Password { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression("^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")]
        public string Email { get; set; }
    }
}