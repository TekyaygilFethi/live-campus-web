﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserProfilePhoto
{
    public class ProfilePhotoFormData
    {
        public int ID { get; set; }

        public string Base64Image { get; set; }
    }
}