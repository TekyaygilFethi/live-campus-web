﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController.Profile
{
    public class UserProfileEducationFormData
    {
        public UserProfileEducationFormData()
        {
            Universities = new List<UserProfileEducationUniversityFormData>();
        }

        public ICollection<UserProfileEducationUniversityFormData> Universities { get; set; }

        public int EducationID { get; set; }

    }
}