﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController.Profile
{
    public class UserProfileTopicFormData
    {
        public string TopicName { get; set; }

        public int TopicID { get; set; }

        public int FavCount { get; set; }
    }
}