﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController.Profile
{
    public class UserProfileConnectionsFormData
    {
        public int ID { get; set; }

        public string ConnectionName { get; set; }

        public string OpponentUserName { get; set; }

        public DateTime ConnectionDate { get; set; }

        public TimeSpan ConnectionDuration { get; set; }
    }
}