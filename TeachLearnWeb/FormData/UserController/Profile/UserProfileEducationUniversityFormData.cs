﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController.Profile
{
    public class UserProfileEducationUniversityFormData
    {
        public UserProfileEducationUniversityFormData()
        {
            Faculties = new List<UserProfileEducationFacultyFormData>();
        }

        public string UniversityName { get; set; }

        public ICollection<UserProfileEducationFacultyFormData> Faculties { get; set; }

        public int LessonCount { get; set; }

        public double AverageGrade { get; set; }
    }
}