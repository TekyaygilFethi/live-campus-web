﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController.Profile
{
    public class UserProfilePostsFormData
    {
        public int TopicID { get; set; }

        public int PostID { get; set; }

        public string TopicName { get; set; }

        public string PostName { get; set; }

        public int FavCount { get; set; }
    }
}