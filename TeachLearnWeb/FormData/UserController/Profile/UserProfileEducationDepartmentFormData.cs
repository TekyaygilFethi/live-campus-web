﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController.Profile
{
    public class UserProfileEducationDepartmentFormData
    {
        public int ID { get; set; }

        public string DepartmentName { get; set; }

        public int LessonCount { get; set; }

        public double AverageGrade { get; set; }

        public int Year { get; set; }

        public int Term { get; set; }
    }
}