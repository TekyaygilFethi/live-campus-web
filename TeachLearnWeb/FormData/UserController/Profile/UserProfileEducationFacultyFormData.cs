﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController.Profile
{
    public class UserProfileEducationFacultyFormData
    {
        public UserProfileEducationFacultyFormData()
        {
            Departments = new List<UserProfileEducationDepartmentFormData>();
        }
        public int ID { get; set; }

        public string FacultyName { get; set; }

        public ICollection<UserProfileEducationDepartmentFormData> Departments { get; set; }

        public int LessonCount { get; set; }

        public double AverageGrade { get; set; }
    }
}