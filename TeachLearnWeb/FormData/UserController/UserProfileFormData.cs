﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.FormData.UserController.Profile;

namespace TeachLearnWeb.FormData.UserController
{
    public class UserProfileFormData
    {
        public UserProfileFormData()
        {
            FavPosts = new List<UserProfilePostsFormData>();
            FavTopics = new List<UserProfileTopicFormData>();

            SentPosts = new List<UserProfilePostsFormData>();
            SentTopics = new List<UserProfileTopicFormData>();

            Connections = new List<UserProfileConnectionsFormData>();

            Educations = new List<UserProfileEducationFormData>();
            IsSentRequest = false;
            IsFriend = false;
        }
        public int? ParameteredUserID { get; set; }

        public bool IsSentRequest { get; set; }

        public bool IsFriend { get; set; }

        public string ProfilePhotoBase64 { get; set; }

        public string Username { get; set; }

        public string NameSurname { get; set; }

        public DateTime Birthday { get; set; }
        
        public TimeSpan TotalConnectionDuration { get; set; }

        public int ConnectionCount { get; set; }

        public int LessonCount { get; set; }

        public double AverageGradeOverall { get; set; }

        public ICollection<UserProfilePostsFormData> FavPosts { get; set; }

        public ICollection<UserProfileTopicFormData> FavTopics { get; set; }

        public ICollection<UserProfilePostsFormData> SentPosts { get; set; }

        public ICollection<UserProfileTopicFormData> SentTopics { get; set; }

        public ICollection<UserProfileConnectionsFormData> Connections { get; set; }

        public ICollection<UserProfileEducationFormData> Educations { get; set; }

    }
}