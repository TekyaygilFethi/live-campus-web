﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnWeb.FormData.UserController
{
    public class AllFriendRequestsFormData
    {
        public List<FriendRequestsFormData> SentRequests { get; set; }

        public List<FriendRequestsFormData> ReceivedRequests { get; set; }
    }
}