﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.UserController
{
    public class FriendRequestsFormData
    {
        public UserProfilePhoto.ProfilePhotoFormData UserProfilePhotoFormData { get; set; }

        public User RequestedUser { get; set; }
    }
}