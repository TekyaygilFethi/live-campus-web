﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.AdminController
{
    public class AllDepartmentsFormData
    {
        public int StudentCount { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }
}