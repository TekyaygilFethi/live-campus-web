﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.FormData.ChatController
{
    public class ChatFormData
    {
        public string SenderUsername { get; set; }

        public string ReceiverUsername { get; set; }

        public string SenderNameSurname { get; set; }

        public string ReceiverNameSurname { get; set; }

        public string SenderConnectionID { get; set; }

        public string ReceiverConnectionID { get; set; }

        public string SenderPPBase64 { get; set; }

        public string ReceiverPPBase64 { get; set; }

        public List<Message> Messages { get; set; }
        
    }
}