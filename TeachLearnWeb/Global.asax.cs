﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.Controllers;


namespace TeachLearnWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected async void Session_End(object sender, EventArgs e)
        {
            (Session["Student"] as Student).User.IsOnline = false;
            await UserController._uow.SaveChanges();
            //using (TeachLearnWeb.Data.DbContextFolder.TeachLearnWebDbContext entities = new Data.DbContextFolder.TeachLearnWebDbContext())
            //{
            //    
            //    entities.SaveChanges();
            //}


        }
    }
}
