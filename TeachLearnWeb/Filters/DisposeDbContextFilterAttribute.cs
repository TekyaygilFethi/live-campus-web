﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.BusinessFolder;

namespace TeachLearnWeb.Filters
{
    public class DisposeDbContextFilterAttribute : ActionFilterAttribute
    {
        private static readonly WebDbContextHelper webContextHelper = new WebDbContextHelper();
        private static readonly LogDbContextHelper logContextHelper = new LogDbContextHelper();

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            webContextHelper.DisposeContext();
            logContextHelper.DisposeContext();
        }

    }
}