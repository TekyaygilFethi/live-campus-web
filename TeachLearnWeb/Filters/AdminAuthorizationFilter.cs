﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.Filters
{
    public class AdminAuthorizationFilter : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!(filterContext.HttpContext.Session["Student"] as Student).User.IsAdmin)
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Error", action = "Index" }));
            }

        }
    }
}