﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TeachLearnWeb.Startup))]
namespace TeachLearnWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //DevExpress.Web.ASPxWebControl.GlobalTheme = "Office2010Silver";
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
