﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.DbContextFolder;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using Microsoft.AspNet.SignalR.Hubs;

namespace TeachLearnWeb.SignalR
{
    [HubName("signalRChatHub")]
    public class ChatHub : Hub
    {
        static TeachLearnWebDbContext context = new TeachLearnWebDbContext();
        static UnitOfWork _uow = new UnitOfWork(context);

        IRepository<User> _userRepository = _uow.GetRepository<User>();
        IRepository<Conversation> _conversationRepository = _uow.GetRepository<Conversation>();
        IRepository<Message> _messageRepository = _uow.GetRepository<Message>();

        User User=null;



        public void BroadCastMessage() { }

        [HubMethodName("hubconnect")]
        public async Task Get_Connect(string username, string namesurname, string UserConnectionID)
        {
            try
            {
                User = await _userRepository.SingleGetByAsync(w => w.Username == username);

                User.SignalRConnectionID = Context.ConnectionId;
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            //var ConnectionID = Context.ConnectionId;
            //string[] Exceptional = new string[1];
            //Exceptional[0] = ConnectionID;
            //Clients.AllExcept(Exceptional).receiveMessage("NewConnection", username + " " + ConnectionID);
        }

        public async Task Send_PrivateMessage(string senderUsername, string message, string receiverUsername)
        {

            //Clients.Client(targetUser).receiveMessage(senderUsername,message);



            User SenderUser = await _userRepository.SingleGetByAsync(w => w.Username == senderUsername);
            User TargetUser = await _userRepository.SingleGetByAsync(w => w.Username == receiverUsername);
            
            Message newMessage = new Message();
            newMessage.MessageContent = message;
            newMessage.Sender = SenderUser;
            newMessage.SendTime = DateTime.Now;

            Conversation conversation = null;
            try
            {
                conversation = await _conversationRepository.SingleGetByAsync(w => w.Participants.Select(S => S.ID).Contains(SenderUser.ID) && w.Participants.Select(s => s.ID).Contains(TargetUser.ID) && w.Participants.Count == 2);
            }
            catch (Exception ex)
            {

            }

            if (conversation == null)
            {
                conversation = new Conversation();
                conversation.Participants.Add(SenderUser);
                conversation.Participants.Add(TargetUser);
                conversation.StartTime = newMessage.SendTime;
                conversation.Messages.Add(newMessage);

                SenderUser.Conversations.Add(conversation);
                TargetUser.Conversations.Add(conversation);

                await _conversationRepository.AddAsync(conversation);


            }
            else
            {
                await _conversationRepository.AttachAsync(conversation);
                conversation.Messages.Add(newMessage);
            }

            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            var ID = Context.ConnectionId;
            Clients.Caller.receiveMessage(senderUsername, message,"Receiver");
            Clients.Client(TargetUser.SignalRConnectionID).receiveMessage(senderUsername, message,"Sender");
        }

        public override  System.Threading.Tasks.Task OnConnected()
        {
            string ClientID = Context.ConnectionId;
            Clients.Caller.receiveMessage((User==null? "" :User.Username), ClientID,"Receiver");

            return base.OnConnected();
        }

        public override System.Threading.Tasks.Task OnReconnected()
        {
            return base.OnReconnected();
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            //disconnected oldu.
            string ClientId = Context.ConnectionId;

            return base.OnDisconnected(stopCalled);
        }

        //public void Send(string UserID, string message)
        //{
        //    int userID = int.Parse(UserID);
        //    using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
        //    {
        //        User user = entities.Users.SingleOrDefault(w => w.ID == userID);
        //        if (user == null)
        //        {
        //            Clients.Caller.showErrorMessage("Could not find that user!");
        //        }
        //        else
        //        {

        //        }
        //    }

        //    //Clients.Group(who).addChatMesssage(name+":"+message);
        //}
        //public override Task OnConnected()
        //{
        //    string name = Context.User.Identity.Name;

        //    Groups.Add(Context.ConnectionId, name);

        //    return base.OnConnected();
        //}

    }
}