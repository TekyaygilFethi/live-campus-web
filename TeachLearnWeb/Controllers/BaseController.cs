﻿using System;
using System.Web.Mvc;
using TeachLearnWeb.BusinessFolder;

namespace TeachLearnWeb.Controllers
{
    public class BaseController : AsyncController,IDisposable
    {
         static public WebDbContextHelper _contextHelper = new WebDbContextHelper();

         static public Business.UnitOfWork _uow = new Business.UnitOfWork(_contextHelper.GetContext());

        static public int ForumDepartmentIDCNT = 0;
        
    }
}