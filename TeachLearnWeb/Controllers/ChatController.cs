﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.FormData.ChatController;

namespace TeachLearnWeb.Controllers
{
    public class ChatController : BaseController
    {
        IRepository<Conversation> _conversationRepository = _uow.GetRepository<Conversation>();
        IRepository<Message> _messageRepository = _uow.GetRepository<Message>();

        public ActionResult AddConversation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddConversation(Conversation _conversation)
        {
            return View();
        }

        [HttpPost]
        public ActionResult SeeConversation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DeleteConversation()
        {
            return View();
        }

        public ActionResult AddMessage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddMessage(Message _Message)
        {
            return View();
        }

        [HttpPost]
        public ActionResult SeeMessage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DeleteMessage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FavMessage()
        {
            return View();
        }



        public ActionResult Deneme()
        {
            return View();
        }

        [HttpGet]
        public async Task<PartialViewResult> ChatPanel(int userID)
        {
            IRepository<User> _userRepository = _uow.GetRepository<User>();
            User receiverUser;

            receiverUser = await _userRepository.SingleGetByAsync(w => w.ID == userID);

            Student currentStudent = HttpContext.Session["Student"] as Student;
            User senderUser = currentStudent.User;

            var a= Convert.ToBase64String(senderUser.ProfilePhoto);
            var b = Convert.ToBase64String(receiverUser.ProfilePhoto);
            
            Conversation conversation = conversation = await _conversationRepository.SingleGetByAsync(w => w.Participants.Select(S => S.ID).Contains(receiverUser.ID) && w.Participants.Select(s => s.ID).Contains(senderUser.ID) && w.Participants.Count == 2); ;

            List<Message> Messages = conversation == null ? new List<Message>():conversation.Messages.OrderByDescending(O=>O.SendTime).Take(15).ToList();


            return PartialView(new ChatFormData
            {
                SenderUsername = senderUser.Username,
                ReceiverUsername = receiverUser.Username,
                SenderConnectionID = senderUser.SignalRConnectionID,
                ReceiverConnectionID = receiverUser.SignalRConnectionID,
                SenderNameSurname = currentStudent.NameSurname,
                ReceiverNameSurname = receiverUser.Student.NameSurname,
                SenderPPBase64 = "data:image/png;base64," + Convert.ToBase64String(senderUser.ProfilePhoto),
                ReceiverPPBase64 = "data:image/png;base64," + Convert.ToBase64String(receiverUser.ProfilePhoto),
                Messages = Messages
            });
        }

        public PartialViewResult FriendsPanelPartial()
        {
            return PartialView("FriendsPanelPartial", GetOnlineFriends());
        }

        public IEnumerable<User> GetOnlineFriends()
        {
            var list= (HttpContext.Session["Student"] as Student)
                .User.Friends.Where(w => w.IsOnline == true);

            List<User> OnlineFriends = new List<User>();

            foreach(var item in list)
            {
                OnlineFriends.Add(item);
            }

            return OnlineFriends.AsEnumerable();

        }
    }
}