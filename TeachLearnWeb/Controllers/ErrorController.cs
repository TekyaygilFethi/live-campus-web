﻿using System.Web.Mvc;

namespace TeachLearnWeb.Controllers
{
    [AllowAnonymous]
    public class ErrorController : BaseController
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }
    }
}