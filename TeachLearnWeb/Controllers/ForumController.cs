﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.Filters;
using TeachLearnWeb.FormData.ForumController;
using TeachLearnWeb.FormData.UserProfilePhoto;

namespace TeachLearnWeb.Controllers
{
    public class ForumController : BaseController
    {
        IRepository<ForumDepartment> forumDepartmentRepository = _uow.GetRepository<ForumDepartment>();
        IRepository<ForumFaculty> forumFacultyRepository = _uow.GetRepository<ForumFaculty>();
        IRepository<Post> postRepository = _uow.GetRepository<Post>();
        IRepository<Topic> topicRepository = _uow.GetRepository<Topic>();
        IRepository<User> userRepository = _uow.GetRepository<User>();
        IRepository<SentFeeds> sentfeedsRepository = _uow.GetRepository<SentFeeds>();
        IRepository<FavouriteFeeds> favouriteFeedsRepository = _uow.GetRepository<FavouriteFeeds>();

        #region Read
        public async Task<ActionResult> Index()
        {
            ForumDepartmentIDCNT = 0;

            ForumIndexFormData fifd = new ForumIndexFormData();
            fifd.ForumDepartments = await forumDepartmentRepository.GetAllAsync();
            fifd.ForumFaculties = await forumFacultyRepository.GetAllAsync();

            return View(fifd);
        }

        public async Task<PartialViewResult> FormDepartmentsPartialInitial(int ID, int page) //partial view ı 
        {
            ForumFaculty forumFaculty = await forumFacultyRepository.GetByIdAsync(ID);


            if (forumFaculty.ForumDepartments.Count == 0)
            {
                var topicList = forumFaculty.Topics.Count >= 25 ? forumFaculty.Topics.ToList().GetRange(25 * page, 25) : forumFaculty.Topics.ToList().GetRange(25 * page, forumFaculty.Topics.Count);

                return PartialView("", topicList);
            }
            else
            {
                var topicList = forumFaculty.ForumDepartments.Count >= 25 ? forumFaculty.ForumDepartments.ToList().GetRange(25 * page, 25) : forumFaculty.ForumDepartments.ToList().GetRange(25 * page, forumFaculty.ForumDepartments.Count);
                return PartialView("", topicList);
            }



        }

        [HttpPost]
        public async Task<JsonResult> GetForumDepartments(int ID)
        {
            ForumFaculty selectedForumFaculty = await forumFacultyRepository.GetByIdAsync(ID);

            return Json(new SelectList(selectedForumFaculty.ForumDepartments.AsEnumerable(), "ID", "Name"));
        }

        public async Task<ActionResult> ForumDepartmentTopics(int ID, int? page)//default olarak 15 topic olsun her page de
        {
            ForumDepartmentIDCNT = ID;

            int _page = page == null ? 0 : (int)page;

            int fdID = (int)ID;
            ForumDepartment fd = await forumDepartmentRepository.GetByIdAsync(fdID);

            TempData["Page"] = _page;

            return View(fd);
        }

        public async Task<ActionResult> ForumFacultyDeps(int ID, int? page) //forum faculty
        {
            ForumDepartmentIDCNT = 0;

            ForumFaculty ff = await forumFacultyRepository.GetByIdAsync(ID);

            int _page = page == null ? 0 : ((int)page - 1);

            int fdID = (int)ID;

            TempData["Page"] = _page;

            return View(ff);
        }

        public async Task<ActionResult> TopicPosts(int ID, int? page, int? anchoredPostID)
        {
            Topic topic = await topicRepository.GetByIdAsync(ID);
            ICollection<ProfilePhotoFormData> ppfdList = new List<ProfilePhotoFormData>();

            int _page = page == null ? 0 : ((int)page) - 1;

            int fdID = (int)ID;

            TempData["Page"] = _page;

            if (anchoredPostID != null)
            {
                TempData["anchoredPostID"] = (int)anchoredPostID;

                Post anchoredPost = topic.Posts.SingleOrDefault(w => w.ID == anchoredPostID);

                _page = topic.Posts.IndexOf(anchoredPost) % 15 == 0 ? topic.Posts.IndexOf(anchoredPost) / 15 : (topic.Posts.IndexOf(anchoredPost) / 15);
                TempData["Page"] = _page;

            }
            if (topic.Posts != null)
            {
                foreach (var item in topic.Posts)
                {
                    if (ppfdList.SingleOrDefault(w => w.ID == item.SentFeed.User.ID) == null)
                    {
                        ProfilePhotoFormData ppfd = new ProfilePhotoFormData();
                        ppfd.ID = item.SentFeed.User.ID;
                        ppfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(item.SentFeed.User.ProfilePhoto);
                        ppfdList.Add(ppfd);
                    }
                }
            }
            TopicPageFormData tpfd = new TopicPageFormData();
            tpfd.Topic = topic;
            tpfd.ProfilePhotoFormDatas = ppfdList;
            return View(tpfd);
        }

        [UserAuthorizationFilter]
        public ActionResult SentPostList()
        {
            User currentUser = (Session["Student"] as Student).User;

            return View(currentUser.SentFeeds.SentPosts);
        }

        [UserAuthorizationFilter]
        public ActionResult SentTopicList()
        {
            User currentUser = (Session["Student"] as Student).User;
            return View(currentUser.SentFeeds.SentTopics);
        }

        [UserAuthorizationFilter]
        public ActionResult FavTopicList()
        {
            User currentUser = (Session["Student"] as Student).User;
            return View(currentUser.FavouriteFeeds.FavouriteTopics);
        }

        [UserAuthorizationFilter]
        public ActionResult FavPostList()
        {
            User currentUser = (Session["Student"] as Student).User;
            return View(currentUser.FavouriteFeeds.FavouritePosts);
        }

        #endregion

        #region Create
        [UserAuthorizationFilter]
        public async Task<ActionResult> CreateNewTopic()
        {

            if (ForumDepartmentIDCNT == 0)
            {
                TempData["ForumFaculties"] = (await forumFacultyRepository.GetAllAsync()).Select(s => new SelectListItem
                {
                    Selected = true,
                    Text = s.Name,
                    Value = s.ID.ToString()
                }).ToList();

            }
            else
            {
                ForumDepartment fd = await forumDepartmentRepository.GetByIdAsync(ForumDepartmentIDCNT);

                TempData["ForumDepartment"] = fd;
                TempData["ForumFaculty"] = fd.ForumFaculty;
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateNewTopic(CreateNewTopicFormData cntfd)
        {
            User currentUser = (Session["Student"] as Student).User;
            ForumFaculty currentForumFaculty = await forumFacultyRepository.GetByIdAsync(cntfd.ForumFacultyID);

            ForumDepartment currentForumDepartment = await forumDepartmentRepository.GetByIdAsync(cntfd.ForumDepartmentID);

            await userRepository.AttachAsync(currentUser);
            await forumFacultyRepository.AttachAsync(currentForumFaculty);
            await forumDepartmentRepository.AttachAsync(currentForumDepartment);


            SentFeeds sf = null;

            if (currentUser.SentFeeds == null)
            {
                sf = new SentFeeds();
                sf.User = currentUser;
                currentUser.SentFeeds = sf;
                await sentfeedsRepository.AddAsync(sf);
            }
            else
            {
                sf = currentUser.SentFeeds;
            }

            Topic topic = new Topic();
            topic.Name = cntfd.TopicName;

            topic.SentFeed = sf;
            sf.SentTopics.Add(topic);

            topic.ForumFaculty = currentForumFaculty;
            currentForumFaculty.Topics.Add(topic);

            Post post = new Post();
            post.Content = cntfd.Content;
            post.PostDate = DateTime.Now;

            post.Topic = topic;
            topic.Posts.Add(post);

            post.ForumFaculty = currentForumFaculty;
            currentForumFaculty.Posts.Add(post);

            post.SentFeed = sf;
            sf.SentPosts.Add(post);


            topic.ForumDepartment = currentForumDepartment;
            currentForumDepartment.Topics.Add(topic);


            post.ForumDepartment = currentForumDepartment;
            currentForumDepartment.Posts.Add(post);


            await topicRepository.AddAsync(topic);
            await postRepository.AddAsync(post);

            ForumDepartmentIDCNT = 0;

            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var redirectUrl = new UrlHelper(Request.RequestContext).Action("Index");
            return Json(new { Url = redirectUrl });

        }

        [HttpPost]
        public async Task<ActionResult> SendPost(int topicID, string postContent)
        {
            Topic currentTopic = await topicRepository.GetByIdAsync(topicID);
            User currentUser = (Session["Student"] as Student).User;

            await topicRepository.AttachAsync(currentTopic);
            await userRepository.AttachAsync(currentUser);

            SentFeeds sf = null;

            if (currentUser.SentFeeds == null)
            {
                sf = new SentFeeds();
                sf.User = currentUser;
                currentUser.SentFeeds = sf;
                await sentfeedsRepository.AddAsync(sf);
            }
            else
            {
                sf = currentUser.SentFeeds;
            }

            Post newPost = new Post();
            newPost.Topic = currentTopic;
            newPost.PostDate = DateTime.Now;
            newPost.ForumDepartment = currentTopic.ForumDepartment;
            newPost.ForumFaculty = currentTopic.ForumFaculty;
            newPost.Content = postContent;
            newPost.SentFeed = sf;


            currentTopic.Posts.Add(newPost);
            currentUser.SentFeeds.SentPosts.Add(newPost);

            currentTopic.ForumDepartment.Posts.Add(newPost);
            currentTopic.ForumFaculty.Posts.Add(newPost);


            await postRepository.AddAsync(newPost);

            Thread.Sleep(2000);
            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            return View();
        }

        #endregion

        #region Delete
        [HttpPost]
        public async Task<ActionResult> DeletePost(int ID)
        {
            Post deletedPost = postRepository.GetByIdAsync(ID).Result;
            Topic topic = deletedPost.Topic;
            ForumDepartment fd = deletedPost.ForumDepartment;
            ForumFaculty ff = deletedPost.ForumFaculty;
            SentFeeds sf = deletedPost.SentFeed;

            topic.Posts.Remove(deletedPost);
            fd.Posts.Remove(deletedPost);
            ff.Posts.Remove(deletedPost);
            sf.SentPosts.Remove(deletedPost);

            await postRepository.DeleteAsync(deletedPost);

            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }



            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteTopic(int ID)
        {
            Topic deletedTopic = await topicRepository.GetByIdAsync(ID);
            ForumDepartment fd = deletedTopic.ForumDepartment;
            ForumFaculty ff = deletedTopic.ForumFaculty;
            SentFeeds sf = deletedTopic.SentFeed;


            foreach (var post in deletedTopic.Posts)
            {
                fd.Posts.Remove(post);
                ff.Posts.Remove(post);
                sf.SentPosts.Remove(post);
            }

            await postRepository.DeleteManyAsync(deletedTopic.Posts);

            await topicRepository.DeleteAsync(deletedTopic);

            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex) { }


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        #endregion

        #region Update
        [HttpPost]
        public async Task<ActionResult> UpdatePost(int ID, string content)
        {
            Post post = await postRepository.GetByIdAsync(ID);

            post.Content = content;

            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            { }


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateTopic(int ID, string topicName)
        {
            Topic topic = await topicRepository.GetByIdAsync(ID);
            topic.Name = topicName;

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> ToggleFavTopic(int ID)
        {
            Topic favTopic = await topicRepository.GetByIdAsync(ID);
            User currentUser = (Session["Student"] as Student).User;

            await userRepository.AttachAsync(currentUser);

            FavouriteFeeds ff = null;

            if (currentUser.FavouriteFeeds == null)
            {
                ff = new FavouriteFeeds();
                ff.User = currentUser;
                currentUser.FavouriteFeeds = ff;
                await favouriteFeedsRepository.AddAsync(ff);
            }
            else
            {
                ff = currentUser.FavouriteFeeds;
            }


            if (ff.FavouriteTopics.Contains(favTopic))
            {
                ff.FavouriteTopics.Remove(favTopic);
                favTopic.FavouritedCount--;
            }
            else
            {
                ff.FavouriteTopics.Add(favTopic);
                favTopic.FavouriteFeeds.Add(ff);
                favTopic.FavouritedCount++;
            }


            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }



            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> ToggleFavPost(int ID)
        {
            Post post = await postRepository.GetByIdAsync(ID);

            User currentUser = (Session["Student"] as Student).User;

            await userRepository.AttachAsync(currentUser);

            FavouriteFeeds ff = null;

            if (currentUser.FavouriteFeeds == null)
            {
                ff = new FavouriteFeeds();
                ff.User = currentUser;
                currentUser.FavouriteFeeds = ff;
                await favouriteFeedsRepository.AddAsync(ff);
            }
            else
            {
                ff = currentUser.FavouriteFeeds;
            }


            if (ff.FavouritePosts.Contains(post))
            {
                ff.FavouritePosts.Remove(post);
                post.FavouritedCount--;
            }
            else
            {
                post.FavouriteFeed = ff;
                ff.FavouritePosts.Add(post);
                post.FavouritedCount++;
            }


            try
            {
                await _uow.SaveChanges();


            }
            catch (Exception ex) { }


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<PartialViewResult> EditPostPartial(int ID)
        {
            Post post = await postRepository.GetByIdAsync(ID);

            TempData["Content"] = post.Content;
            TempData["PostID"] = post.ID;

            return PartialView();
        }
        #endregion

    }
}