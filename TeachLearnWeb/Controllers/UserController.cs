﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.FormData;
using TeachLearnWeb.ModelBinders;
using TeachLearnWeb.Security;
using System.Web.Security;
using TeachLearnWeb.Filters;
using TeachLearnWeb.FormData.UserController;
using TeachLearnWeb.FormData.UserController.Profile;
using System.Net;
using TeachLearnWeb.FormData.UserProfilePhoto;

namespace TeachLearnWeb.Controllers
{
    public class UserController : BaseController
    {
        IRepository<Student> _studentRepository = _uow.GetRepository<Student>();
        IRepository<User> _userRepository = _uow.GetRepository<User>();
        IRepository<FriendRelationship> _relationshipRepository = _uow.GetRepository<FriendRelationship>();
        IRepository<FriendRequest> _friendRequestRepository = _uow.GetRepository<FriendRequest>();
        [AllowAnonymous]
        public ActionResult CreateAccount()
        {
            return View(new AccountFormData());
        }

        [HttpPost]
        public async Task<ActionResult> CreateAccount([ModelBinder(typeof(CreateAccountModelBinder))] object[] dataArray)
        {
            if (ModelState.IsValid)
            {
                var _user = dataArray[0] as User;
                var _student = dataArray[1] as Student;


                //_user.FriendsParent = _user;
                //_user.FriendParentID = _user.ID;
                //await _userRepository.AddAsync(_user);

                //FriendRelationship newRelationship = new FriendRelationship();
                //newRelationship.Friend = _user;
                //newRelationship.FriendID = _user.ID;

                //await _relationshipRepository.AddAsync(newRelationship);

                try
                {
                    await _userRepository.AddAsync(_user);
                    await _studentRepository.AddAsync(_student);
                    await _uow.SaveChanges();

                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null) ex = ex.InnerException;
                    ViewBag.Messsage = ex.Message;
                    ViewBag.Info = "danger";
                }
            }
            else
            {
                ViewBag.Message = "Please check again the fields!";
                ViewBag.Info = "danger";
            }
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Login(FormData.LoginFormData _loginCrediental)
        {
            var _userEnteredUsername = _loginCrediental.Username;
            var _userEnteredPassword = _loginCrediental.Password;

            if (await HashSaltClass.CheckCreedientals(_userEnteredUsername, _userEnteredPassword))
            {
                Student _loggedStudent = (await _userRepository.SingleGetByAsync(w => w.Username == _userEnteredUsername)).Student;
                _loggedStudent.User.IsOnline = true;
                Session["Student"] = _loggedStudent;
            }
            else //No account in db
            {
                TempData["Script"] = "alert('Login failed!');";
            }

            return Json("");
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> LogOut()
        {
            (Session["Student"] as Student).User.IsOnline = false;
            await _uow.SaveChanges();
            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("Index", "Home");
        }

        [UserAuthorizationFilter]
        public async Task<ActionResult> UserProfile(int? ID)
        {
            Student currentStudent = (ID == null ? Session["Student"] as Student : await _studentRepository.GetByIdAsync((int)ID));

            User currentUser = currentStudent.User;

            UserProfileFormData upfd = new UserProfileFormData();
            upfd.Username = currentUser.Username;
            upfd.NameSurname = currentStudent.NameSurname;
            upfd.ConnectionCount = currentUser.ConnectionsAsSharer.Count + currentUser.ConnectionsAsViewer.Count;

            upfd.ParameteredUserID = ID == null ? null : ID;

            if (ID != null)
            {
                if ((Session["Student"] as Student).User.SentFriendRequests.Any(w => w.RequestToID == currentUser.ID && w.IsAccepted == false))
                {
                    upfd.IsSentRequest = true;
                }
                else
                {
                    if ((Session["Student"] as Student).User.Friends.Contains(currentUser) && currentUser.Friends.Contains((Session["Student"] as Student).User))
                    {
                        upfd.IsFriend = true;
                    }
                }
            }


            #region EDUCATION
            if (currentStudent.Educations.Count != 0)
            {
                UserProfileEducationFormData upefd = new UserProfileEducationFormData();

                foreach (var education in currentStudent.Educations)
                {
                    upefd.EducationID = education.ID;

                    UserProfileEducationUniversityFormData upeufd = upefd.Universities.SingleOrDefault(w => w.UniversityName == education.University.Name);

                    if (upeufd != null) //university varsa
                    {
                        UserProfileEducationFacultyFormData upeffd = upeufd.Faculties.SingleOrDefault(w => w.FacultyName == education.Faculty.Name);

                        if (upeffd != null)//faculty varsa
                        {
                            UserProfileEducationDepartmentFormData upedfd = upeffd.Departments.SingleOrDefault(w => w.DepartmentName == education.Department.Name);

                            if (upedfd != null)//department varsa
                            {
                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }
                            }
                            else
                            {
                                upedfd = new UserProfileEducationDepartmentFormData();
                                upedfd.DepartmentName = education.Department.Name;
                                upedfd.Term = education.Period.Semester;
                                upedfd.Year = education.Period.Year;

                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }

                                upeffd.Departments.Add(upedfd);
                            }
                        }
                        else
                        {
                            upeffd = new UserProfileEducationFacultyFormData();
                            upeffd.FacultyName = education.Faculty.Name;

                            upeufd.Faculties.Add(upeffd);

                            UserProfileEducationDepartmentFormData upedfd = upeffd.Departments.SingleOrDefault(w => w.DepartmentName == education.Department.Name);

                            if (upedfd != null)//department varsa
                            {
                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }


                            }
                            else
                            {
                                upedfd = new UserProfileEducationDepartmentFormData();
                                upedfd.DepartmentName = education.Department.Name;
                                upedfd.Term = education.Period.Semester;
                                upedfd.Year = education.Period.Year;


                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }

                                upeffd.Departments.Add(upedfd);
                            }


                        }
                    }
                    else
                    {
                        upeufd = new UserProfileEducationUniversityFormData();
                        upeufd.UniversityName = education.University.Name;
                        upefd.Universities.Add(upeufd);


                        UserProfileEducationFacultyFormData upeffd = upeufd.Faculties.SingleOrDefault(w => w.FacultyName == education.Faculty.Name);

                        if (upeffd != null)//faculty varsa
                        {
                            UserProfileEducationDepartmentFormData upedfd = upeffd.Departments.SingleOrDefault(w => w.DepartmentName == education.Department.Name);

                            if (upedfd != null)//department varsa
                            {
                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }


                            }
                            else
                            {
                                upedfd = new UserProfileEducationDepartmentFormData();
                                upedfd.DepartmentName = education.Department.Name;
                                upedfd.Term = education.Period.Semester;
                                upedfd.Year = education.Period.Year;

                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }

                                upeffd.Departments.Add(upedfd);
                            }
                        }
                        else
                        {
                            upeffd = new UserProfileEducationFacultyFormData();
                            upeffd.FacultyName = education.Faculty.Name;

                            upeufd.Faculties.Add(upeffd);

                            UserProfileEducationDepartmentFormData upedfd = upeffd.Departments.SingleOrDefault(w => w.DepartmentName == education.Department.Name);

                            if (upedfd != null)//department varsa
                            {
                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }
                            }
                            else
                            {
                                upedfd = new UserProfileEducationDepartmentFormData();
                                upedfd.DepartmentName = education.Department.Name;
                                upedfd.Term = education.Period.Semester;
                                upedfd.Year = education.Period.Year;


                                upedfd.LessonCount = education.Scores.Count;
                                upeffd.LessonCount = education.Scores.Count;
                                upeufd.LessonCount = education.Scores.Count;

                                foreach (var score in education.Scores)
                                {
                                    foreach (var note in score.Notes)
                                    {
                                        upedfd.AverageGrade += ((note.ResultPoint * note.Effect) / 100);
                                    }
                                }

                                upeffd.Departments.Add(upedfd);
                            }
                        }
                    }


                }
                if (upfd.Educations.SingleOrDefault(w => w.EducationID == upefd.EducationID) == null)
                {
                    upfd.Educations.Add(upefd);
                }
            }


            #endregion

            upfd.ProfilePhotoBase64 = "data:image/png;base64," + Convert.ToBase64String(currentUser.ProfilePhoto);
            upfd.Birthday = currentStudent.Birthday;

            #region FAV FEEDS
            if (currentUser.FavouriteFeeds != null)
            {
                foreach (var favPost in currentUser.FavouriteFeeds.FavouritePosts.Take(5))
                {
                    UserProfilePostsFormData uppfd = new UserProfilePostsFormData();
                    uppfd.FavCount = favPost.FavouritedCount;
                    uppfd.PostName = favPost.Content;
                    uppfd.TopicName = favPost.Topic.Name;
                    uppfd.PostID = favPost.ID;
                    uppfd.TopicID = favPost.TopicID;

                    upfd.FavPosts.Add(uppfd);
                }

                foreach (var favTopic in currentUser.FavouriteFeeds.FavouriteTopics.Take(5))
                {
                    UserProfileTopicFormData uptfd = new UserProfileTopicFormData();
                    uptfd.FavCount = favTopic.FavouritedCount;
                    uptfd.TopicName = favTopic.Name;
                    uptfd.TopicID = favTopic.ID;

                    upfd.FavTopics.Add(uptfd);
                }
            }
            #endregion

            #region SENT FEEDS
            if (currentUser.SentFeeds != null)
            {
                foreach (var sentPost in currentUser.SentFeeds.SentPosts.Take(5))
                {
                    UserProfilePostsFormData uppfd = new UserProfilePostsFormData();
                    uppfd.FavCount = sentPost.FavouritedCount;
                    uppfd.PostName = sentPost.Content;
                    uppfd.TopicName = sentPost.Topic.Name;
                    uppfd.PostID = sentPost.ID;
                    uppfd.TopicID = sentPost.TopicID;

                    upfd.SentPosts.Add(uppfd);
                }

                foreach (var sentTopic in currentUser.SentFeeds.SentTopics.Take(5))
                {
                    UserProfileTopicFormData uptfd = new UserProfileTopicFormData();
                    uptfd.FavCount = sentTopic.FavouritedCount;
                    uptfd.TopicName = sentTopic.Name;
                    uptfd.TopicID = sentTopic.ID;

                    upfd.SentTopics.Add(uptfd);
                }
            }
            #endregion

            if (currentStudent.Educations != null)
            {
                foreach (var education in currentStudent.Educations)
                {
                    upfd.LessonCount += education.Scores.Count;

                    foreach (var score in education.Scores)
                    {
                        upfd.AverageGradeOverall += score.Average;
                    }
                }
            }


            List<Connection> connections = new List<Connection>();
            connections.AddRange(currentUser.ConnectionsAsSharer);
            connections.AddRange(currentUser.ConnectionsAsViewer);

            if (connections.Count != 0)
            {
                foreach (var connection in connections.OrderByDescending(o => o.ConnectionDate).Take(5))
                {
                    UserProfileConnectionsFormData upcfd = new UserProfileConnectionsFormData();
                    upcfd.ID = connection.ID;
                    upcfd.ConnectionName = connection.ConnectionName;
                    upcfd.OpponentUserName = connection.SharerID == currentUser.ID ? connection.Sharer.Username : connection.Viewer.Username;

                    upcfd.ConnectionDate = connection.ConnectionDate;
                    upcfd.ConnectionDuration = connection.ConnectionDuration;

                    upfd.TotalConnectionDuration += connection.ConnectionDuration;

                    upfd.Connections.Add(upcfd);
                }
            }
            return View(upfd);
        }

        [AdminAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> ToggleAdmin(int ID)
        {
            User user = await _userRepository.GetByIdAsync(ID);

            if (user.IsAdmin)
            {
                user.IsAdmin = false;
            }
            else
            {
                user.IsAdmin = true;
            }

            await _uow.SaveChanges();

            return View();
        }

        [AllowAnonymous]
        public ActionResult FAQPage()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UnsendFriendRequest(int userID)
        {
            User requestedToUser = await _userRepository.GetByIdAsync(userID);

            User requesterUser = (Session["Student"] as Student).User;

            FriendRequest fr = await _friendRequestRepository.SingleGetByAsync(w => w.RequesterID == requesterUser.ID && w.RequestToID == userID && w.IsAccepted == false);

            await _friendRequestRepository.DeleteAsync(fr);

            requestedToUser.ReceivedFriendRequests.Remove(fr);
            requesterUser.SentFriendRequests.Remove(fr);

            await _uow.SaveChanges();



            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> SendFriendRequest(int userID)
        {
            User requestedToUser = await _userRepository.GetByIdAsync(userID);

            User requesterUser = (Session["Student"] as Student).User;

            if (!(await _friendRequestRepository.GetByAsync(w => w.RequesterID == requesterUser.ID && w.RequestToID == userID && w.IsAccepted == false)).Any())
            {
                FriendRequest fr = new FriendRequest();
                fr.Requester = requesterUser;
                fr.RequestTo = requestedToUser;
                requestedToUser.ReceivedFriendRequests.Add(fr);
                requesterUser.SentFriendRequests.Add(fr);
                await _friendRequestRepository.AddAsync(fr);

                await _uow.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);




        }

        [HttpPost]
        public async Task<ActionResult> AcceptFriendRequest(int userID)
        {
            User requesterUser = await _userRepository.GetByIdAsync(userID);

            User requestedToUser = (Session["Student"] as Student).User;

            FriendRequest fr = await _friendRequestRepository.SingleGetByAsync(w => w.RequesterID == requesterUser.ID && w.RequestToID == requestedToUser.ID && w.IsAccepted == false);

            fr.IsAccepted = true;

            requestedToUser.Friends.Add(requesterUser);
            requesterUser.Friends.Add(requestedToUser);

            await _uow.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteFriend(int userID)
        {
            User requestedToUser = await _userRepository.GetByIdAsync(userID);

            User requesterUser = (Session["Student"] as Student).User;

            requestedToUser.Friends.Remove(requesterUser);
            requesterUser.Friends.Remove(requestedToUser);


            await _uow.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public async Task<ActionResult> FriendRequests()
        {
            User currentUser = (Session["Student"] as Student).User;

            List<FriendRequestsFormData> SentRequests = new List<FriendRequestsFormData>();
            List<FriendRequestsFormData> ReceivedRequests = new List<FriendRequestsFormData>();
            //List<ProfilePhotoFormData> ppfdList = new List<ProfilePhotoFormData>();



            foreach (var requestedUser in currentUser.SentFriendRequests.Where(w => w.IsAccepted == false).Select(s => s.RequestTo))
            {
                FriendRequestsFormData frfd = new FriendRequestsFormData();

                ProfilePhotoFormData ppfd = new ProfilePhotoFormData();

                ppfd.ID = requestedUser.ID;
                ppfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(requestedUser.ProfilePhoto);


                frfd.UserProfilePhotoFormData = ppfd;
                frfd.RequestedUser = requestedUser;
                //ppfdList.Add(ppfd);

                SentRequests.Add(frfd);
            }

            foreach (var requestTo in currentUser.ReceivedFriendRequests.Where(w => w.IsAccepted == false).Select(s => s.Requester))
            {
                FriendRequestsFormData frfd = new FriendRequestsFormData();

                ProfilePhotoFormData ppfd = new ProfilePhotoFormData();

                ppfd.ID = requestTo.ID;
                ppfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(requestTo.ProfilePhoto);


                frfd.UserProfilePhotoFormData = ppfd;
                frfd.RequestedUser = requestTo;
                //ppfdList.Add(ppfd);

                ReceivedRequests.Add(frfd);
            }

            AllFriendRequestsFormData afrfd = new AllFriendRequestsFormData();
            afrfd.SentRequests = SentRequests;
            afrfd.ReceivedRequests = ReceivedRequests;

            return View(afrfd);
        }

        [HttpPost]
        public async Task<JsonResult> GetAllUsernames()
        {
            var usernameList = (await _userRepository.GetAllAsync()).Take(5).Select(s => s.Username).ToList();

            return Json(usernameList);

        }

        [HttpPost]
        public async Task<JsonResult> GetIDByUsername(string username)
        {
            int? ID = (await _userRepository.SingleGetByAsync(w => w.Username == username)).ID == (Session["Student"] as Student).User.ID ? null : (int?)(await _userRepository.SingleGetByAsync(w => w.Username == username)).ID;

            if (ID == null)
            {
                return Json(String.Empty);
            }
            else
            {
                return Json(ID);
            }

        }
    }
}