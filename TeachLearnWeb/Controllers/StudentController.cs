﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.FormData.StudentController;
using TeachLearnWeb.Data.DbContextFolder;
using TeachLearnWeb.Business;
using System.Threading.Tasks;
using TeachLearnWeb.Data.UniversityModel;
using System.Collections;
using TeachLearnWeb.Filters;
using TeachLearnWeb.BusinessFolder;
using TeachLearnWeb.ModelBinders;

namespace TeachLearnWeb.Controllers
{

    public class StudentController : BaseController
    {
        //static WebDbContextHelper _contextHelper = new WebDbContextHelper();
        //static UnitOfWork _uow = new UnitOfWork(_contextHelper.GetContext());

        #region Repositories
        IRepository<Faculty> _facultyRepository = _uow.GetRepository<Faculty>();
        IRepository<Department> _departmentRepository = _uow.GetRepository<Department>();
        IRepository<University> _universityRepository = _uow.GetRepository<University>();
        IRepository<Education> _educationRepository = _uow.GetRepository<Education>();
        IRepository<Period> _periodRepository = _uow.GetRepository<Period>();
        IRepository<Student> _studentRepository = _uow.GetRepository<Student>();
        IRepository<Lesson> _lessonRepository = _uow.GetRepository<Lesson>();
        IRepository<Score> _scoreRepository = _uow.GetRepository<Score>();
        IRepository<Note> _noteRepository = _uow.GetRepository<Note>();
        IRepository<ForumDepartment> _forumDepartmentRepository = _uow.GetRepository<ForumDepartment>();
        #endregion

        #region Education Ops
        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<JsonResult> GetFaculties(string universityID)
        {
            int _universityID = int.Parse(universityID);
            var faculties = (await _facultyRepository.GetByAsync(w => w.UniversityID == _universityID)).ToList();

            return Json(new SelectList(faculties, "ID", "Name"));
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<JsonResult> GetDepartments(string facultyID)
        {
            int _facultyID = int.Parse(facultyID);
            var departments = await _departmentRepository.GetByAsync(w => w.FacultyID == _facultyID);

            return Json(new SelectList(departments, "ID", "Name"));
        }

        [UserAuthorizationFilter]
        public async Task<ActionResult> AddUniversity()
        {
            List<int> years = new List<int>(6);
            List<int> terms = new List<int>(2);

            for (int i = 1; i <= 6; i++)
            {
                years.Add(i);
                if (i <= 2)
                {
                    terms.Add(i);
                }
            }

            TempData["Years"] = years.Select(s => new SelectListItem
            {
                Value = s.ToString(),
                Text = s.ToString(),
                Selected = true
            }).ToList();

            TempData["Terms"] = terms.Select(s => new SelectListItem
            {
                Value = s.ToString(),
                Text = s.ToString(),
                Selected = true
            }).ToList();


            TempData["Universities"] = (await _universityRepository.GetAllAsync()).Select(s => new SelectListItem
            {
                Value = s.ID.ToString(),
                Text = s.Name,
                Selected = true
            }).ToList();

            return View();

        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> AddUniversity(AddUniversityFormData _auFormData)//formdata faculty ve department
        {
            int _semester = int.Parse(_auFormData.Semester);
            int _year = int.Parse(_auFormData.Year);
            int _universityID = int.Parse(_auFormData.University);
            int _facultyID = int.Parse(_auFormData.Faculty);
            int _departmentID = int.Parse(_auFormData.Department);

            Student _currentStudent = HttpContext.Session["Student"] as Student;
            University _university = await _universityRepository.SingleGetByAsync(w => w.ID == _universityID);
            Faculty _faculty = await _facultyRepository.SingleGetByAsync(w => w.ID == _facultyID);
            Department _department = await _departmentRepository.SingleGetByAsync(w => w.ID == _departmentID);

            await _studentRepository.AttachAsync(_currentStudent);
            await _universityRepository.AttachAsync(_university);
            await _facultyRepository.AttachAsync(_faculty);
            await _departmentRepository.AttachAsync(_department);

            Period _period = await _periodRepository.SingleGetByAsync(w => w.Semester == _semester && w.Year == _year);

            Education _newEducation = new Education();

            _newEducation.University = _university;
            _newEducation.Faculty = _faculty;
            _newEducation.Department = _department;
            _newEducation.Student = _currentStudent;
            _newEducation.Period = _period;

            _period.Educations.Add(_newEducation);
            _currentStudent.Educations.Add(_newEducation);
            _university.Educations.Add(_newEducation);
            _faculty.Educations.Add(_newEducation);
            _department.Educations.Add(_newEducation);

            await _educationRepository.AddAsync(_newEducation);
            await _periodRepository.AttachAsync(_period);


            await _universityRepository.UpdateAsync(_university);
            await _facultyRepository.UpdateAsync(_faculty);
            await _departmentRepository.UpdateAsync(_department);

            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) ex = ex.InnerException;
                ViewBag.Messsage = ex.Message;
                ViewBag.Info = "danger";
            }

            return RedirectToAction("ListEducations");
        }

        [UserAuthorizationFilter]
        public async Task<ActionResult> ListEducations()
        {
            Student _currentStudent = Session["Student"] as Student;

            var _educationList = await _educationRepository.GetByAsync(w => w.StudentID == _currentStudent.ID);

            return View(_educationList);
        }

        [UserAuthorizationFilter]
        public async Task<ActionResult> SeeEducationDetails(int _educationID)
        {
            Education _education = await _educationRepository.SingleGetByAsync(w => w.ID == _educationID);
            if ((Session["Student"] as Student).Educations.Contains(_education))
            {
                return View(_education);
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> DeleteEducation(int _educationID)
        {
            Education _deletedEducation = await _educationRepository.SingleGetByAsync(w => w.ID == _educationID);

            Student _currentStudent = Session["Student"] as Student;
            _currentStudent.Educations.Remove(_deletedEducation);

            List<Score> _deletedScores = _deletedEducation.Scores;
            List<Note> _deletedNotes = new List<Note>();

            foreach (var score in _deletedScores)
            {
                _deletedNotes.AddRange(score.Notes);
            }

            try
            {
                await _noteRepository.DeleteManyAsync(_deletedNotes);
                await _scoreRepository.DeleteManyAsync(_deletedScores);
                await _educationRepository.DeleteAsync(_deletedEducation);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("ListEducations");
        }

        [UserAuthorizationFilter]
        public async Task<ActionResult> EditEducation(int _educationID)
        {
            List<int> years = new List<int>(6);
            List<int> terms = new List<int>(2);

            for (int i = 1; i <= 6; i++)
            {
                years.Add(i);
                if (i <= 2)
                {
                    terms.Add(i);
                }
            }

            TempData["Years"] = years.Select(s => new SelectListItem
            {
                Value = s.ToString(),
                Text = s.ToString(),
                Selected = true
            }).ToList();

            TempData["Terms"] = terms.Select(s => new SelectListItem
            {
                Value = s.ToString(),
                Text = s.ToString(),
                Selected = true
            }).ToList();

            TempData["Universities"] = (await _universityRepository.GetAllAsync()).Select(s => new SelectListItem
            {
                Value = s.ID.ToString(),
                Text = s.Name,
                Selected = true
            }).ToList();

            Education _editedEducation = await _educationRepository.SingleGetByAsync(w => w.ID == _educationID);
            return View(_editedEducation);
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> EditEducation(EditUniversityFormData _euFormData)
        {
            int _semester = int.Parse(_euFormData.Semester);
            int _year = int.Parse(_euFormData.Year);
            int _universityID = int.Parse(_euFormData.University);
            int _facultyID = int.Parse(_euFormData.Faculty);
            int _departmentID = int.Parse(_euFormData.Department);

            Education _editedEducation = await _educationRepository.SingleGetByAsync(w => w.ID == _euFormData.ID);
            Period _editedPeriod = await _periodRepository.SingleGetByAsync(w => w.Year == _year && w.Semester == _semester);

            University _university = await _universityRepository.SingleGetByAsync(w => w.ID == _universityID);
            Faculty _faculty = await _facultyRepository.SingleGetByAsync(w => w.ID == _facultyID);
            Department _department = await _departmentRepository.SingleGetByAsync(w => w.ID == _departmentID);

            _editedEducation.University = _university;
            _editedEducation.UniversityID = _universityID;

            _editedEducation.Faculty = _faculty;
            _editedEducation.FacultyID = _facultyID;

            _editedEducation.Department = _department;
            _editedEducation.DepartmentID = _departmentID;

            _university.Educations.Add(_editedEducation);
            _faculty.Educations.Add(_editedEducation);
            _department.Educations.Add(_editedEducation);


            _editedEducation.Period = _editedPeriod;
            _editedEducation.PeriodID = _editedPeriod.ID;

            foreach (var score in _editedEducation.Scores)
            {
                score.Lesson.Department = _department;
                await _lessonRepository.UpdateAsync(score.Lesson);
            }

            try
            {
                await _educationRepository.UpdateAsync(_editedEducation);
                await _universityRepository.UpdateAsync(_university);
                await _facultyRepository.UpdateAsync(_faculty);
                await _departmentRepository.UpdateAsync(_department);

                await _uow.SaveChanges();

            }
            catch (Exception ex)
            {

            }



            return RedirectToAction("SeeEducationDetails", new { _educationID = _euFormData.ID });
        }
        #endregion

        #region Lesson Ops
        [UserAuthorizationFilter]
        public async Task<ActionResult> AddLesson(int _educationID)
        {
            TempData["Education"] = _educationID.ToString();

            List<ForumDepartment> fdList = (await _forumDepartmentRepository.GetAllAsync()).ToList();

            TempData["Categories"] = (await _forumDepartmentRepository.GetAllAsync()).Select(s => new SelectListItem
            {
                Value = s.ID.ToString(),
                Text = s.Name.ToString(),
                Selected = true
            }).ToList();

            return View(new Lesson());
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> AddLesson([ModelBinder(typeof(AddLessonModelBinder))]string[] parameters)
        {
            string lessonName = parameters[0];
            int categoryID = int.Parse(parameters[1]);

            ForumDepartment _categoryForumDepartment = await _forumDepartmentRepository.GetByIdAsync(categoryID);

            await _forumDepartmentRepository.AttachAsync(_categoryForumDepartment);


            int _educationId = int.Parse(TempData["Education"].ToString());
            Education _currentEducation = await _educationRepository.SingleGetByAsync(w => w.ID == _educationId);

            await _educationRepository.AttachAsync(_currentEducation);

            Lesson newLesson = new Lesson();
            newLesson.Name = lessonName;
            newLesson.Department = _currentEducation.Department;
            newLesson.Period = _currentEducation.Period;

            Score newScore = new Score();
            newScore.Education = _currentEducation;
            newScore.Lesson = newLesson;

            newScore.ForumDepartmentCategory = _categoryForumDepartment;

            _categoryForumDepartment.Scores.Add(newScore);

            _currentEducation.Scores.Add(newScore);
            try
            {
                await _scoreRepository.AddAsync(newScore);
                await _lessonRepository.AddAsync(newLesson);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) ex = ex.InnerException;
                ViewBag.Messsage = ex.Message;
                ViewBag.Info = "danger";
            }
            return RedirectToAction("SeeEducationDetails", new { _educationID = _educationId });
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> DeleteLesson(int _scoreID)
        {
            Score _score = await _scoreRepository.SingleGetByAsync(w => w.ID == _scoreID);

            Education _parentEducation = _score.Education;
            Lesson _deletedLesson = _score.Lesson;
            IEnumerable<Note> _deletedNotes = _score.Notes;

            _parentEducation.Scores.Remove(_score);

            try
            {
                await _lessonRepository.DeleteAsync(_deletedLesson);
                await _noteRepository.DeleteManyAsync(_deletedNotes);
                await _scoreRepository.DeleteAsync(_score);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            //string ReturnUrl = "../Student/SeeEducationDetails?_educationID=" + _parentEducation.ID;
            //return Json(ReturnUrl);
            return RedirectToActionPermanent("SeeEducationDetails", new { _educationID = _parentEducation.ID });
        }

        [UserAuthorizationFilter]
        public async Task<ActionResult> EditLesson(int _lessonID)
        {
            Lesson _editedLesson = await _lessonRepository.SingleGetByAsync(w => w.ID == _lessonID);

            return View(_editedLesson);
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> EditLesson(Lesson _modelLesson)
        {
            Lesson _updatedLesson = await _lessonRepository.SingleGetByAsync(w => w.ID == _modelLesson.ID);
            _updatedLesson.Name = _modelLesson.Name;


            return RedirectToAction("ListEducations");
        }
        #endregion

        #region Note Ops
        [UserAuthorizationFilter]
        public ActionResult AddNote(int _scoreID)
        {
            TempData["Score"] = _scoreID.ToString();
            return View(new Note());
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> AddNote(Note newNote)
        {
            int _scoreID = int.Parse(TempData["Score"].ToString());
            Score _currentScore = await _scoreRepository.SingleGetByAsync(W => W.ID == _scoreID);

            await _scoreRepository.AttachAsync(_currentScore);

            newNote.Score = _currentScore;
            _currentScore.Notes.Add(newNote);

            foreach (var note in _currentScore.Notes)
            {
                _currentScore.Average += ((note.ResultPoint * note.Effect) / 100);
            }

            try
            {
                await _noteRepository.AddAsync(newNote);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) ex = ex.InnerException;
                ViewBag.Messsage = ex.Message;
                ViewBag.Info = "danger";
            }


            return RedirectToAction("SeeEducationDetails", new { _educationID = _currentScore.EducationID });
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public ActionResult EditNote(Note _editedNote)
        {
            return View();
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> DeleteNote(int _noteID)
        {
            Note _deletedNote = await _noteRepository.SingleGetByAsync(w => w.ID == _noteID);
            Score _parentScore = _deletedNote.Score;

            _parentScore.Average = 0;
            _parentScore.Notes.Remove(_deletedNote);

            try
            {
                await _noteRepository.DeleteAsync(_deletedNote);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("SeeEducationDetails", new { _educationID = _parentScore.EducationID });
        }
        #endregion

        [UserAuthorizationFilter]
        #region Score Ops
        public async Task<ActionResult> EditScore(int _scoreID)
        {
            Score _score = await _scoreRepository.SingleGetByAsync(w => w.ID == _scoreID);

            return View(_score.Notes);
        }

        [UserAuthorizationFilter]
        [HttpPost]
        public async Task<ActionResult> EditScore(Note _newNote)
        {
            Note _updatedNote = await _noteRepository.SingleGetByAsync(w => w.ID == _newNote.ID);

            _updatedNote.Description = _newNote.Description;
            _updatedNote.ResultPoint = _newNote.ResultPoint;
            _updatedNote.Effect = _newNote.Effect;
            try
            {
                await _noteRepository.UpdateAsync(_updatedNote);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("SeeEducationDetails", new { _educationID = _updatedNote.Score.EducationID });
        }

        #endregion

        #region Grade Competititon
        [AllowAnonymous]
        public async Task<ActionResult> GradeCompetition(int? ID, string filterLevel = "All")
        {
            TempData["Header"] = "All Students Grade Rankings";
            List<Student> students = null;
            if (ID == null)
            {
                students = (await _studentRepository.GetAllAsync()).ToList();
            }
            else
            {
                switch (filterLevel)
                {
                    case "University":
                        TempData["Header"] = (await _universityRepository.GetByIdAsync((int)ID)).Name;
                        foreach (var student in await _studentRepository.GetAllAsync())
                        {
                            foreach (var education in student.Educations)
                            {
                                if (education.UniversityID == (int)ID)
                                {
                                    students.Add(student);
                                }
                            }
                        }

                        break;

                    case "Faculty":
                        TempData["Header"] = (await _facultyRepository.GetByIdAsync((int)ID)).Name;
                        foreach (var student in await _studentRepository.GetAllAsync())
                        {
                            foreach (var education in student.Educations)
                            {
                                if (education.FacultyID == (int)ID)
                                {
                                    students.Add(student);
                                }
                            }
                        }
                        break;

                    case "Department":
                        TempData["Header"] = (await _departmentRepository.GetByIdAsync((int)ID)).Name;
                        foreach (var student in await _studentRepository.GetAllAsync())
                        {
                            foreach (var education in student.Educations)
                            {
                                if (education.DepartmentID == (int)ID)
                                {
                                    students.Add(student);
                                }
                            }
                        }
                        break;

                    case "All":
                        students = (await _studentRepository.GetAllAsync()).ToList();
                        break;

                }
            }

            List<GradeCompetitionFormData> gcfdList = new List<GradeCompetitionFormData>();
            int order = 1;
            foreach (var student in students)
            {
                GradeCompetitionFormData gcfd = new GradeCompetitionFormData();
                gcfd.Student = student;

                double totalAvg = 0;

                foreach (var education in student.Educations)
                {
                    if (education.Scores.Count != 0)
                    {
                        foreach (var score in education.Scores)
                        {
                            totalAvg += score.Average;
                        }

                        if (totalAvg > gcfd.TotalAverage)
                        {
                            gcfd.TotalAverage = totalAvg;
                            gcfd.UniversityName = education.University.Name;
                            gcfd.FacultyName = education.Faculty.Name;
                            gcfd.DepartmentName = education.Department.Name;
                        }
                        gcfd.TotalAverage /= education.Scores.Count;
                    }
                }
                gcfdList.Add(gcfd);
            }

            foreach (var gcfd in gcfdList.OrderByDescending(o => o.TotalAverage))
            {
                gcfd.Order = order;
                order++;
            }


            return View(gcfdList.OrderBy(o => o.Order));
        }

        #endregion
    }
}