﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.Filters;
using TeachLearnWeb.FormData.ConnectionController;
using TeachLearnWeb.FormData.UserProfilePhoto;

namespace TeachLearnWeb.Controllers
{
    [UserAuthorizationFilter]
    public class ConnectionController : BaseController
    {
        IRepository<Connection> connectionRepository = _uow.GetRepository<Connection>();
        IRepository<User> userRepository = _uow.GetRepository<User>();
        IRepository<Review> reviewRepository = _uow.GetRepository<Review>();
        IRepository<ScreenShareRequest> screenShareRequestRepository = _uow.GetRepository<ScreenShareRequest>();

        [HttpPost]
        public async Task<ActionResult> ConnectAsSharer()
        {
            int userID = (Session["Student"] as Student).ID;
            User sharerUser = await userRepository.SingleGetByAsync(w => w.ID == userID);


            if (sharerUser.ConnectionsAsViewer.Where(w => w.IsConnectionEnded == false && w.IsConnectionStarted == false).Count() != 0)
            {
                sharerUser.ConnectionsAsViewer.Where(w => w.IsConnectionEnded == false && w.IsConnectionStarted == false).ToList().ForEach(each => each.IsConnectionEnded = true);
            }


            sharerUser.PendingStatus = "Sharer";

            await _uow.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> ConnectAsViewer()
        {
            int userID = (Session["Student"] as Student).ID;
            User sharerUser = await userRepository.SingleGetByAsync(w => w.ID == userID);


            if (sharerUser.ConnectionsAsSharer.Where(w => w.IsConnectionEnded == false && w.IsConnectionStarted == false).Count() != 0)
            {
                sharerUser.ConnectionsAsSharer.Where(w => w.IsConnectionEnded == false && w.IsConnectionStarted == false).ToList().ForEach(each => each.IsConnectionEnded = true);

            }

            sharerUser.PendingStatus = "Viewer";

            await _uow.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult PreviousConnections()
        {
            if (Session["Student"] != null)
            {
                User currentUser = (Session["Student"] as Student).User;

                List<Connection> Connections = new List<Connection>();
                Connections.AddRange(currentUser.ConnectionsAsSharer);
                Connections.AddRange(currentUser.ConnectionsAsViewer);

                return View(Connections);
            }
            else
            {
                return View();
            }
        }

        public async Task<ActionResult> ConnectionDetails(int ID)
        {

            Connection conn = await connectionRepository.GetByIdAsync(ID);

            if ((Session["Student"] as Student).User.ID == conn.SharerID || (Session["Student"] as Student).User.ID == conn.ViewerID)
            {
                TempData["Unauthorized"] = true;
            }
                ConnectionDetailsFormData cdfd = new ConnectionDetailsFormData();
                cdfd.Connection = conn;





                foreach (var review in cdfd.Connection.Reviews)
                {
                    ProfilePhotoFormData ppfd = new ProfilePhotoFormData();

                    ppfd.ID = review.Sender.ID;
                    ppfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(review.Sender.ProfilePhoto);


                    cdfd.ProfilePhotoFormDatas.Add(ppfd);

                }

                return View(cdfd);
            

        }

        public async Task<PartialViewResult> EditConnectionNamePartial(int ID)
        {
            Connection connection = await connectionRepository.GetByIdAsync(ID);

            TempData["ConnectionName"] = connection.ConnectionName;
            TempData["ConnectionID"] = connection.ID;
            return PartialView();
        }

        [HttpPost]
        public async Task<ActionResult> UpdateConnectionName(int ID, string name)
        {
            Connection connection = await connectionRepository.GetByIdAsync(ID);

            connection.ConnectionName = name;

            await _uow.SaveChanges();


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }


        public async Task<ActionResult> CreateReview(int ID)
        {
            Connection connection = await connectionRepository.GetByIdAsync(ID);

            return View(connection);
        }

        [HttpPost]
        public async Task<ActionResult> CreateReview(int ID, string review, string Vote = "upvote")
        {
            Connection currentConnection = await connectionRepository.GetByIdAsync(ID);
            await connectionRepository.AttachAsync(currentConnection);

            Review newReview = new Review();
            newReview.Connection = currentConnection;
            newReview.Message = review;
            newReview.Sender = (Session["Student"] as Student).User;

            if (Vote == "downvote")
            {
                newReview.Vote = Data.POCOs.TeachLearnWeb.Vote.DownVote;
            }
            else
                newReview.Vote = Data.POCOs.TeachLearnWeb.Vote.Upvote;

            currentConnection.Reviews.Add(newReview);

            await reviewRepository.AddAsync(newReview);

            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> EstablishPreviousConnection(int ID, string status)
        {
            User currentUser = (Session["Student"] as Student).User;
            //currentUser.PendingStatus = "Sharer";
            if (!currentUser.SentScreenShareRequests.Where(w => w.IsAccepted == false && w.NewConnection.IsConnectionEnded == false).Any())
            {
                Connection conn = await connectionRepository.GetByIdAsync(ID);

                User partnerUser = conn.ViewerID == currentUser.ID ? conn.Sharer : conn.Viewer;

                ScreenShareRequest ssRequest = new ScreenShareRequest();
                ssRequest.Requester = currentUser;
                ssRequest.RequestTo = partnerUser;

                currentUser.SentScreenShareRequests.Add(ssRequest);
                partnerUser.ReceivedScreenShareRequests.Add(ssRequest);

                ssRequest.RequesterRequestType = status == "Sharer" ? RequestType.Sharer : RequestType.Viewer;

                ssRequest.RequestToRequestType = ssRequest.RequesterRequestType == RequestType.Sharer ? RequestType.Viewer : RequestType.Sharer;

                ssRequest.NewConnection = conn;

                //Connection newConnection = new Connection();
                //newConnection.ConnectionName = conn.ConnectionName.Contains("Repeat") ? conn.ConnectionName : conn.ConnectionName + " (Repeat)";
                //newConnection.ForumDepartmentCategory = conn.ForumDepartmentCategory;
                //newConnection.ConnectionDate = DateTime.Now;

                //conn.ForumDepartmentCategory.Connections.Add(newConnection);

                //if (status == "Sharer")
                //{
                //    currentUser.PendingStatus = "Sharer";
                //    newConnection.Sharer = currentUser;
                //    currentUser.ConnectionsAsSharer.Add(newConnection);
                //}
                //else
                //{
                //    currentUser.PendingStatus = "Viewer";
                //    newConnection.Viewer = currentUser;
                //    currentUser.ConnectionsAsViewer.Add(newConnection);
                //}

                //ssRequest.RequestedConnection = conn;
                //conn.BaseRequests.Add(ssRequest);

                //ssRequest.NewConnection = newConnection;
                //newConnection.BaseRequests.Add(ssRequest);

                await screenShareRequestRepository.AddAsync(ssRequest);
                //await connectionRepository.AddAsync(newConnection);

                try
                {
                    await _uow.SaveChanges();
                }
                catch (Exception ex) { }
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "You already have an existing connection pending!");


        }

        public ActionResult PendingConnection()
        {
            Connection pendingConnection = (Session["Student"] as Student).User.ConnectionsAsSharer.SingleOrDefault(w => w.IsConnectionEnded == false);
            pendingConnection = pendingConnection == null ? (Session["Student"] as Student).User.ConnectionsAsViewer.SingleOrDefault(w => w.IsConnectionEnded == false) : pendingConnection;


            return View(pendingConnection);
        }

        [HttpPost]
        public async Task<ActionResult> DeletePendingConnection(int ID)
        {
            Connection connection = await connectionRepository.GetByIdAsync(ID);

            await connectionRepository.DeleteAsync(connection);

            await _uow.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public async Task<ActionResult> EditReview(int ID)
        {
            Review editedReview = await reviewRepository.GetByIdAsync(ID);

            return View(editedReview);
        }

        [HttpPost]
        public async Task<ActionResult> EditReview(int ID, string review, string Vote = "upvote")
        {
            Review editedReview = await reviewRepository.GetByIdAsync(ID);
            editedReview.Message = review;

            if (Vote == "downvote")
                editedReview.Vote = Data.POCOs.TeachLearnWeb.Vote.DownVote;

            else
                editedReview.Vote = Data.POCOs.TeachLearnWeb.Vote.Upvote;

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

    }
}