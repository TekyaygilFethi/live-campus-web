﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using System.Web.Helpers;
using System.Collections;
using TeachLearnWeb.FormData.AdminController;
using System.Net;
using TeachLearnWeb.Filters;

namespace TeachLearnWeb.Controllers
{
    [UserAuthorizationFilter]
    [AdminAuthorizationFilter]
    public class AdminController : BaseController
    {
        IRepository<User> userRepository = _uow.GetRepository<User>();
        IRepository<SentFeeds> sentFeedsRepository = _uow.GetRepository<SentFeeds>();
        IRepository<FavouriteFeeds> favouriteFeedsRepository = _uow.GetRepository<FavouriteFeeds>();
        IRepository<Faculty> facultyRepository = _uow.GetRepository<Faculty>();
        IRepository<Department> departmentRepository = _uow.GetRepository<Department>();
        IRepository<University> universityRepository = _uow.GetRepository<University>();
        IRepository<Connection> connectionRepository = _uow.GetRepository<Connection>();
        IRepository<ForumDepartment> forumDepartmentRepository = _uow.GetRepository<ForumDepartment>();
        IRepository<ForumFaculty> forumFacultyRepository = _uow.GetRepository<ForumFaculty>();
        IRepository<Topic> topicRepository = _uow.GetRepository<Topic>();
        IRepository<Post> postRepository = _uow.GetRepository<Post>();
        IRepository<Contact> contactRepository = _uow.GetRepository<Contact>();

        public ActionResult Index()
        {

            //new Chart(width: 600, height: 200, theme: ChartTheme.Vanilla3D)
            //    .AddTitle("Chart").AddSeries();

            return View();
        }

        public async Task<ActionResult> AllUsers(int? ID, string educationLevel = "University")
        {
            if (ID == null)
            {
                TempData["Header"] = "";
                return View(await userRepository.GetAllAsync());
            }
            else
            {
                List<User> userList = new List<User>();
                switch (educationLevel)
                {
                    case "University":
                        TempData["Header"] = "~" + (await universityRepository.GetByIdAsync((int)ID)).Name + "~";

                        foreach (var education in (await universityRepository.GetByIdAsync((int)ID)).Educations)
                        {
                            userList.Add(education.Student.User);
                        }
                        return View(userList.Distinct());


                    case "Faculty":
                        TempData["Header"] = "~" + (await facultyRepository.GetByIdAsync((int)ID)).Name + "~";

                        foreach (var education in (await facultyRepository.GetByIdAsync((int)ID)).Educations)
                        {
                            userList.Add(education.Student.User);
                        }
                        return View(userList.Distinct());

                    case "Department":
                        TempData["Header"] = "~" + (await departmentRepository.GetByIdAsync((int)ID)).Name + "~";

                        foreach (var education in (await departmentRepository.GetByIdAsync((int)ID)).Educations)
                        {
                            userList.Add(education.Student.User);
                        }
                        return View(userList.Distinct());

                }

                return View();
            }



        }

        #region Forum
        public async Task<ActionResult> AllForumFaculties()
        {
            return View(await forumFacultyRepository.GetAllAsync());
        }

        public async Task<ActionResult> AllForumDepartments(int? ID)
        {
            TempData["Header"] = "";
            if (ID == null)
            {
                return View(await forumDepartmentRepository.GetAllAsync());
            }
            else
            {
                TempData["Header"] = "~" + (await forumFacultyRepository.GetByIdAsync((int)ID)).Name + "~";
                return View(await forumDepartmentRepository.GetByAsync(w => w.ForumFacultyID == (int)ID));
            }

        }

        public async Task<ActionResult> AllSentTopics(int? ID, string educationLevel = "ForumFaculty")
        {
            TempData["Header"] = "";
            if (ID == null)
            {
                List<Topic> SentTopics = new List<Topic>();
                foreach (var sentFeeds in (await sentFeedsRepository.GetAllAsync()).Select(s => s.SentTopics))
                {
                    SentTopics.AddRange(sentFeeds);
                }
                return View(SentTopics);
            }
            else
            {
                switch (educationLevel)
                {
                    case "ForumFaculty":
                        TempData["Header"] = (await forumFacultyRepository.GetByIdAsync((int)ID)).Name;
                        return View((await forumFacultyRepository.GetByIdAsync((int)ID)).Topics);


                    case "ForumDepartment":

                        TempData["Header"] = (await forumDepartmentRepository.GetByIdAsync((int)ID)).Name;
                        return View((await forumDepartmentRepository.GetByIdAsync((int)ID)).Topics);
                }
            }

            return View();
        }

        public async Task<ActionResult> AllSentPosts(int? ID, string educationLevel = "ForumFaculty")
        {
            TempData["Header"] = "";
            if (ID == null)
            {
                List<Post> SentPosts = new List<Post>();

                foreach (var sentPosts in (await sentFeedsRepository.GetAllAsync()).Select(s => s.SentPosts))
                {
                    SentPosts.AddRange(sentPosts);
                }
                return View(SentPosts);
            }
            else
            {
                switch (educationLevel)
                {
                    case "ForumFaculty":
                        TempData["Header"] = "~" + (await forumFacultyRepository.GetByIdAsync((int)ID)).Name + "~";
                        return View((await forumFacultyRepository.GetByIdAsync((int)ID)).Posts);


                    case "ForumDepartment":

                        TempData["Header"] = "~" + (await forumDepartmentRepository.GetByIdAsync((int)ID)).Name + "~";
                        return View((await forumDepartmentRepository.GetByIdAsync((int)ID)).Posts);
                }
            }
            return View();

        }
        #endregion

        #region Education
        public async Task<ActionResult> AllUniversities()
        {
            return View(await universityRepository.GetAllAsync());
        }

        public async Task<ActionResult> AllFaculties(int? ID)
        {
            if (ID == null)
            {
                TempData["Header"] = "";
                return View(await facultyRepository.GetAllAsync());
            }

            else
            {
                int universityID = (int)ID;

                TempData["Header"] = "~" + (await universityRepository.GetByIdAsync(universityID)).Name + "~";

                return View(await facultyRepository.GetByAsync(w => w.UniversityID == universityID));
            }
        }

        public async Task<ActionResult> AllDepartments(int? ID, string educationLevel = "University")
        {
            if (ID == null)
            {
                TempData["Header"] = "";
                return View(await departmentRepository.GetAllAsync());
            }
            else
            {
                switch (educationLevel)
                {
                    case "University":
                        int universityID = (int)ID;
                        TempData["Header"] = "~" + (await universityRepository.GetByIdAsync(universityID)).Name + "~";

                        return View((await universityRepository.GetByIdAsync(universityID)).Departments);

                    case "Faculty":
                        int facultyID = (int)ID;
                        TempData["Header"] = "~" + (await facultyRepository.GetByIdAsync(facultyID)).Name + "~";

                        return View((await facultyRepository.GetByIdAsync(facultyID)).Departments);
                }
            }
            return View();
        }
        #endregion

        public async Task<ActionResult> UserTopicStatistics()
        {
            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            var results = await userRepository.GetAllAsync();
            results.ToList().ForEach(rs => xValue.Add(rs.SentFeeds.SentTopics));
            results.ToList().ForEach(rs => yValue.Add(rs.SentFeeds.User.Username));



            new Chart(width: 600, height: 200, theme: ChartTheme.Vanilla3D)
                .AddTitle("User Topic Statistics")
                .AddSeries("Default", chartType: "Bar", xValue: xValue, yValues: yValue)
                .Write("bmp");


            return null;
        }

        public async Task<ActionResult> AllConections()
        {
            return View(await connectionRepository.GetAllAsync());
        }

        public async Task<ActionResult> AllGrades()
        {



            return View();
        }



        public ActionResult AddForumFaculty()
        {
            return View(new ForumFaculty());
        }



        [HttpPost]
        public async Task<ActionResult> AddForumFaculty(ForumFaculty forumFaculty)
        {
            ForumDepartment fd = new ForumDepartment();
            fd.Name = forumFaculty.Name;

            fd.ForumFaculty = forumFaculty;
            forumFaculty.ForumDepartments.Add(fd);

            try
            {
                await forumFacultyRepository.AddAsync(forumFaculty);
                await forumDepartmentRepository.AddAsync(fd);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {
                
            }
            return View();
        }


        public async Task<ActionResult> ForumFacultyUpdatePage(int ID)
        {
            ForumFaculty updatedForumFaculty = await forumFacultyRepository.GetByIdAsync(ID);

            return View(updatedForumFaculty);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateForumFacultyName(int ID, string name)
        {
            ForumFaculty updatedForumFaculty = await forumFacultyRepository.GetByIdAsync(ID);
            updatedForumFaculty.Name = name;
            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteForumFaculty(int ID)
        {
            ForumFaculty deletedForumFaculty = await forumFacultyRepository.GetByIdAsync(ID);

            try
            {
                await forumDepartmentRepository.DeleteManyAsync(deletedForumFaculty.ForumDepartments);
                await topicRepository.DeleteManyAsync(deletedForumFaculty.Topics);
                await postRepository.DeleteManyAsync(deletedForumFaculty.Posts);

                await forumFacultyRepository.DeleteAsync(deletedForumFaculty);
                await _uow.SaveChanges();

            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }




        public async Task<ActionResult> AddForumDepartment()
        {
            var forumFaculties = (await forumFacultyRepository.GetAllAsync()).Select(s => new SelectListItem
            {
                Selected = true,
                Text = s.Name,
                Value = s.ID.ToString(),
            }).ToList();

            TempData["ForumFaculties"] = forumFaculties;
            return View(new ForumDepartment());
        }


        [HttpPost]
        public async Task<ActionResult> AddForumDepartment(string name, int forumFacultyID)
        {
            ForumDepartment addedForumDepartment = new ForumDepartment();
            addedForumDepartment.Name = name;

            ForumFaculty ff = await forumFacultyRepository.GetByIdAsync(forumFacultyID);

            await forumFacultyRepository.AttachAsync(ff);

            addedForumDepartment.ForumFaculty = ff;
            ff.ForumDepartments.Add(addedForumDepartment);
            try
            {
                await forumDepartmentRepository.AddAsync(addedForumDepartment);
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }


        public async Task<ActionResult> ForumDepartmentUpdatePage(int ID)
        {
            ForumDepartment updatedForumDepartment = await forumDepartmentRepository.GetByIdAsync(ID);

            return View(updatedForumDepartment);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteForumDepartment(int ID)
        {
            ForumDepartment deletedForumDepartment = await forumDepartmentRepository.GetByIdAsync(ID);

            try
            {
                await topicRepository.DeleteManyAsync(deletedForumDepartment.Topics);
                await postRepository.DeleteManyAsync(deletedForumDepartment.Posts);
                deletedForumDepartment.ForumFaculty = null;

                await forumDepartmentRepository.DeleteAsync(deletedForumDepartment);
                await _uow.SaveChanges();

            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateForumDepartmentName(int ID, string name)
        {
            ForumDepartment updatedForumDepartent = await forumDepartmentRepository.GetByIdAsync(ID);
            updatedForumDepartent.Name = name;
            try
            {
                await _uow.SaveChanges();
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public async Task<ActionResult> AllContacts()
        {
            return View(await contactRepository.GetAllAsync());
        }

    }
}