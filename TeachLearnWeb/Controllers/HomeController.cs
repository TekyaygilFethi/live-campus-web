﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.Controllers
{
    //[RequireHttps]
    public class HomeController : BaseController
    {
        IRepository<Contact> contactRepository = _uow.GetRepository<Contact>();


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Contact(string Name,string Email,string Message)
        {
            TeachLearnWeb.Data.POCOs.TeachLearnWeb.Contact newContact = new Data.POCOs.TeachLearnWeb.Contact();

            newContact.NameSurname = Name;
            newContact.Email = Email;
            newContact.Message = Message;

            await contactRepository.AddAsync(newContact);

            await _uow.SaveChanges();


            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }


        public async Task<ActionResult> ContactDetails(int ID)
        {
            
            return View(await contactRepository.GetByIdAsync(ID));
        }

        public ActionResult DesktopApplicationTutorialPage()
        {
            return View();
        }


    }
}