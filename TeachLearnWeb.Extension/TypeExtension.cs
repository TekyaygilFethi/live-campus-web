﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Extension
{
    public static class TypeExtension
    {
        public static T MapTo<T>(this object source)
        {
            Type targetType = typeof(T);
            Type sourceType = source.GetType();

            T result = Activator.CreateInstance<T>();

            PropertyInfo[] targetProperties = targetType.GetProperties();
            PropertyInfo[] sourceProperties = targetType.GetProperties();

            foreach(PropertyInfo sp in sourceProperties)
            {
                PropertyInfo tp = targetProperties.FirstOrDefault(x => x.Name.ToLowerInvariant() == sp.Name.ToLower());

                if(tp!=null)
                {
                    object data = sp.GetValue(source);
                    tp.SetValue(result, data);
                }
            }
            return result;

        }
    }
}
