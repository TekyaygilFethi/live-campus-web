﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachLearnWeb.Data.POCOs.Logs;

namespace TeachLearnWeb.Data.DbContextFolder
{
    public class TeachLearnWebLogDbContext : DbContext
    {
        public TeachLearnWebLogDbContext() : base("TeachLearnLogDatabase") { }

        public DbSet<ActionLog> ActionLogs { get; set; }
        public DbSet<DesktopLog> DesktopLogs { get; set; }
        public DbSet<ExceptionLog> ExceptionLogs { get; set; }

    }
}
