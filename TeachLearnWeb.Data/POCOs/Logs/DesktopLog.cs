﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.Logs
{
    [Table("DesktopLogTable")]
    public class DesktopLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int ConnectionID { get; set; }

        public DateTime Time { get; set; }

        public string ExceptionDescription { get; set; }
    }
}
