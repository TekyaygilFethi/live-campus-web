﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.Logs
{
    [Table("ActionLogTable")]
    public class ActionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int UserID { get; set; }

        public string ActionName { get; set; }

        public string ControllerName { get; set; }

        public DateTime Date { get; set; }

        public string Type { get; set; }

    }
}
