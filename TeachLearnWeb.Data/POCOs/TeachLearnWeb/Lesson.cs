﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("LessonTable")]
    public class Lesson
    {
        public Lesson()
        {
            Educations = new List<Education>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }

        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }

        [ForeignKey("PeriodID")]
        public virtual Period Period { get; set; }

        public virtual List<Education> Educations { get; set; }

        public int DepartmentID { get; set; }

        public int? PeriodID { get; set; }
    }
}
