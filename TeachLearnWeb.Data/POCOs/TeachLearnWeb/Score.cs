﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("ScoreTable")]
    public class Score
    {
        public Score()
        {
            Notes = new List<Note>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("LessonID")]
        public virtual Lesson Lesson { get; set; }

        [ForeignKey("EducationID")]
        public virtual Education Education { get; set; }

        public int LessonID { get; set; }

        public int EducationID { get; set; }

        public virtual List<Note> Notes { get; set; }

        public double Average { get; set; }

        [ForeignKey("ForumDepartmentCategoryID")]
        public virtual ForumDepartment ForumDepartmentCategory { get; set; }

        public int? ForumDepartmentCategoryID { get; set; }
    }
}
