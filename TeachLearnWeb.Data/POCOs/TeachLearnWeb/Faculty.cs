﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("FacultyTable")]
    public class Faculty
    {
        public Faculty()
        {
            Departments = new List<Department>();
            Educations = new List<Education>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }

        [ForeignKey("UniversityID")]
        public virtual University University { get; set; }

        public virtual List<Department> Departments { get; set; }

        public virtual List<Education> Educations { get; set; } //şaibeli

        public int UniversityID { get; set; }
    }
}
