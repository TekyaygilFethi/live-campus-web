﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("ForumDepartmentTable")]
    public class ForumDepartment
    {
        public ForumDepartment()
        {
            Topics = new List<Topic>();
            Posts = new List<Post>();
            Connections = new List<Connection>();
            Scores = new List<Score>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Index(IsUnique =true)]
        [MaxLength(100)]
        public string Name { get; set; }

        [ForeignKey("ForumFacultyID")]
        public virtual ForumFaculty ForumFaculty { get; set; }

        public int ForumFacultyID { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<Connection> Connections { get; set; }

        public virtual List<Score> Scores { get; set; }
    }
}
