﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("EducationTable")]
    public class Education
    {
        public Education()
        {
            Scores = new List<Score>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("UniversityID")]
        public virtual University University { get; set; }

        [ForeignKey("FacultyID")]
        public virtual Faculty Faculty { get; set; }

        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }

        [ForeignKey("StudentID")]
        public virtual Student Student { get; set; }

        [ForeignKey("PeriodID")]
        public virtual Period Period { get; set; } //şaibeli

        public virtual List<Score> Scores { get; set; }

        public int UniversityID { get; set; }

        public int FacultyID { get; set; }

        public int DepartmentID { get; set; }

        public int StudentID { get; set; }

        public int PeriodID { get; set; }
    }
}
