﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("ForumFacultyTable")]
    public class ForumFaculty
    {
        public ForumFaculty()
        {
            ForumDepartments = new List<ForumDepartment>();
            Topics = new List<Topic>();
            Posts = new List<Post>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(100)]
        public string Name { get; set; }

        public virtual ICollection<ForumDepartment> ForumDepartments { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
