﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("TopicTable")]
    public class Topic
    {
        public Topic()
        {
            Posts = new List<Post>();
            FavouriteFeeds = new List<FavouriteFeeds>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }

        public virtual List<Post> Posts { get; set; }

        public virtual List<FavouriteFeeds> FavouriteFeeds { get; set; }

        [ForeignKey("SentFeedID")]
        public virtual SentFeeds SentFeed { get; set; }

        public int SentFeedID { get; set; }

        [ForeignKey("ForumFacultyID")]
        public virtual ForumFaculty ForumFaculty { get; set; }

        public int ForumFacultyID { get; set; }

        [ForeignKey("ForumDepartmentID")]
        public virtual ForumDepartment ForumDepartment { get; set; }

        public int? ForumDepartmentID { get; set; }

        public int FavouritedCount { get; set; }

        //[ForeignKey("UserID")]
        //public virtual User Sender { get; set; }

        //public int UserID { get; set; }
    }
}
