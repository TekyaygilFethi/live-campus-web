﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnWeb.Data.POCOs.TeachLearnWeb
{
    [Table("DepartmentTable")]
    public class Department
    {
        public Department()
        {
            Educations = new List<Education>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }

        public virtual List<Education> Educations { get; set; } //şaibeli

        [ForeignKey("FacultyID")]
        public virtual Faculty Faculty { get; set; }

        [ForeignKey("UniversityID")]
        public virtual University University { get; set; }

        public int FacultyID { get; set; }

        public int UniversityID { get; set; }
    }
}
