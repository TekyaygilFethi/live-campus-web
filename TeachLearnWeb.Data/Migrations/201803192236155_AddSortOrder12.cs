namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostTable", "FavouritedCount", c => c.Int(nullable: false));
            AddColumn("dbo.TopicTable", "FavouritedCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TopicTable", "FavouritedCount");
            DropColumn("dbo.PostTable", "FavouritedCount");
        }
    }
}
