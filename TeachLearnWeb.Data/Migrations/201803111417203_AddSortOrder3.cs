namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ForumDepartmentTable", "Name", c => c.String(maxLength: 100));
            AlterColumn("dbo.ForumFacultyTable", "Name", c => c.String(maxLength: 100));
            CreateIndex("dbo.ForumDepartmentTable", "Name", unique: true);
            CreateIndex("dbo.ForumFacultyTable", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.ForumFacultyTable", new[] { "Name" });
            DropIndex("dbo.ForumDepartmentTable", new[] { "Name" });
            AlterColumn("dbo.ForumFacultyTable", "Name", c => c.String());
            AlterColumn("dbo.ForumDepartmentTable", "Name", c => c.String());
        }
    }
}
