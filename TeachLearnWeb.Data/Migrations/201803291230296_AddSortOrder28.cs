namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder28 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FriendRequestTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RequesterID = c.Int(nullable: false),
                        RequestToID = c.Int(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.RequestToID)
                .ForeignKey("dbo.UserTable", t => t.RequesterID)
                .Index(t => t.RequesterID)
                .Index(t => t.RequestToID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FriendRequestTable", "RequesterID", "dbo.UserTable");
            DropForeignKey("dbo.FriendRequestTable", "RequestToID", "dbo.UserTable");
            DropIndex("dbo.FriendRequestTable", new[] { "RequestToID" });
            DropIndex("dbo.FriendRequestTable", new[] { "RequesterID" });
            DropTable("dbo.FriendRequestTable");
        }
    }
}
