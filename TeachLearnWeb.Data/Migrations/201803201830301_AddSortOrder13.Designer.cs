// <auto-generated />
namespace TeachLearnWeb.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddSortOrder13 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddSortOrder13));
        
        string IMigrationMetadata.Id
        {
            get { return "201803201830301_AddSortOrder13"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
