namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenShareRequestTable", "RequesterID", c => c.Int(nullable: false));
            CreateIndex("dbo.ScreenShareRequestTable", "RequesterID");
            AddForeignKey("dbo.ScreenShareRequestTable", "RequesterID", "dbo.UserTable", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenShareRequestTable", "RequesterID", "dbo.UserTable");
            DropIndex("dbo.ScreenShareRequestTable", new[] { "RequesterID" });
            DropColumn("dbo.ScreenShareRequestTable", "RequesterID");
        }
    }
}
