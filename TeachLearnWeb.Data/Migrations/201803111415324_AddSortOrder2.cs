namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ForumDepartmentTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ForumFacultyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ForumFacultyTable", t => t.ForumFacultyID, cascadeDelete: true)
                .Index(t => t.ForumFacultyID);
            
            CreateTable(
                "dbo.ForumFacultyTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForumDepartmentTable", "ForumFacultyID", "dbo.ForumFacultyTable");
            DropIndex("dbo.ForumDepartmentTable", new[] { "ForumFacultyID" });
            DropTable("dbo.ForumFacultyTable");
            DropTable("dbo.ForumDepartmentTable");
        }
    }
}
