namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder19 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectionTable", "ForumDepartmentCategoryID", c => c.Int(nullable: true));
            AddColumn("dbo.ScoreTable", "ForumDepartmentCategoryID", c => c.Int(nullable: true));
            CreateIndex("dbo.ConnectionTable", "ForumDepartmentCategoryID");
            CreateIndex("dbo.ScoreTable", "ForumDepartmentCategoryID");
            AddForeignKey("dbo.ScoreTable", "ForumDepartmentCategoryID", "dbo.ForumDepartmentTable", "ID");
            AddForeignKey("dbo.ConnectionTable", "ForumDepartmentCategoryID", "dbo.ForumDepartmentTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConnectionTable", "ForumDepartmentCategoryID", "dbo.ForumDepartmentTable");
            DropForeignKey("dbo.ScoreTable", "ForumDepartmentCategoryID", "dbo.ForumDepartmentTable");
            DropIndex("dbo.ScoreTable", new[] { "ForumDepartmentCategoryID" });
            DropIndex("dbo.ConnectionTable", new[] { "ForumDepartmentCategoryID" });
            DropColumn("dbo.ScoreTable", "ForumDepartmentCategoryID");
            DropColumn("dbo.ConnectionTable", "ForumDepartmentCategoryID");
        }
    }
}
