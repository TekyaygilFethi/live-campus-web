namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectionTable", "ConnectionDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ConnectionTable", "ConnectionDuration", c => c.Time(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConnectionTable", "ConnectionDuration");
            DropColumn("dbo.ConnectionTable", "ConnectionDate");
        }
    }
}
