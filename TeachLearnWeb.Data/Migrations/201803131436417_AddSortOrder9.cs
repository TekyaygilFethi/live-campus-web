namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder9 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PostTable", new[] { "FavouriteFeedID" });
            AlterColumn("dbo.PostTable", "FavouriteFeedID", c => c.Int());
            CreateIndex("dbo.PostTable", "FavouriteFeedID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PostTable", new[] { "FavouriteFeedID" });
            AlterColumn("dbo.PostTable", "FavouriteFeedID", c => c.Int(nullable: false));
            CreateIndex("dbo.PostTable", "FavouriteFeedID");
        }
    }
}
