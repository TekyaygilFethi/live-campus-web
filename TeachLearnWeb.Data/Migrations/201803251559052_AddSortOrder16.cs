namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder16 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ReviewTable", "UserID", "dbo.UserTable");
            AddColumn("dbo.ReviewTable", "ConnectionID", c => c.Int(nullable: false));
            CreateIndex("dbo.ReviewTable", "ConnectionID");
            AddForeignKey("dbo.ReviewTable", "ConnectionID", "dbo.ConnectionTable", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ReviewTable", "UserID", "dbo.UserTable", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReviewTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.ReviewTable", "ConnectionID", "dbo.ConnectionTable");
            DropIndex("dbo.ReviewTable", new[] { "ConnectionID" });
            DropColumn("dbo.ReviewTable", "ConnectionID");
            AddForeignKey("dbo.ReviewTable", "UserID", "dbo.UserTable", "ID");
        }
    }
}
