namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder8 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PostTable", "PostTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PostTable", "PostTime", c => c.DateTime(nullable: false));
        }
    }
}
