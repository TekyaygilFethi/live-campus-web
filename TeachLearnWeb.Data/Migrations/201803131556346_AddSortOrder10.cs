namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder10 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PostTable", new[] { "ForumDepartmentID" });
            DropIndex("dbo.TopicTable", new[] { "ForumDepartmentID" });
            AlterColumn("dbo.PostTable", "ForumDepartmentID", c => c.Int());
            AlterColumn("dbo.TopicTable", "ForumDepartmentID", c => c.Int());
            CreateIndex("dbo.PostTable", "ForumDepartmentID");
            CreateIndex("dbo.TopicTable", "ForumDepartmentID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.TopicTable", new[] { "ForumDepartmentID" });
            DropIndex("dbo.PostTable", new[] { "ForumDepartmentID" });
            AlterColumn("dbo.TopicTable", "ForumDepartmentID", c => c.Int(nullable: false));
            AlterColumn("dbo.PostTable", "ForumDepartmentID", c => c.Int(nullable: false));
            CreateIndex("dbo.TopicTable", "ForumDepartmentID");
            CreateIndex("dbo.PostTable", "ForumDepartmentID");
        }
    }
}
