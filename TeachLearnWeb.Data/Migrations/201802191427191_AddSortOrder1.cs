namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ConnectionTable", "ViewerID", "dbo.UserTable");
            DropForeignKey("dbo.ConnectionTable", "SharerID", "dbo.UserTable");
            DropIndex("dbo.ConnectionTable", new[] { "ViewerID" });
            DropIndex("dbo.ConnectionTable", new[] { "SharerID" });
            AddColumn("dbo.ConnectionTable", "User_ID", c => c.Int());
            AlterColumn("dbo.ConnectionTable", "ViewerID", c => c.Int());
            AlterColumn("dbo.ConnectionTable", "SharerID", c => c.Int());
            CreateIndex("dbo.ConnectionTable", "ViewerID");
            CreateIndex("dbo.ConnectionTable", "SharerID");
            CreateIndex("dbo.ConnectionTable", "User_ID");
            AddForeignKey("dbo.ConnectionTable", "ViewerID", "dbo.UserTable", "ID");
            AddForeignKey("dbo.ConnectionTable", "User_ID", "dbo.UserTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConnectionTable", "User_ID", "dbo.UserTable");
            DropForeignKey("dbo.ConnectionTable", "ViewerID", "dbo.UserTable");
            DropIndex("dbo.ConnectionTable", new[] { "User_ID" });
            DropIndex("dbo.ConnectionTable", new[] { "SharerID" });
            DropIndex("dbo.ConnectionTable", new[] { "ViewerID" });
            AlterColumn("dbo.ConnectionTable", "SharerID", c => c.Int(nullable: false));
            AlterColumn("dbo.ConnectionTable", "ViewerID", c => c.Int(nullable: false));
            DropColumn("dbo.ConnectionTable", "User_ID");
            CreateIndex("dbo.ConnectionTable", "SharerID");
            CreateIndex("dbo.ConnectionTable", "ViewerID");
            AddForeignKey("dbo.ConnectionTable", "SharerID", "dbo.UserTable", "ID");
            AddForeignKey("dbo.ConnectionTable", "ViewerID", "dbo.UserTable", "ID", cascadeDelete: false);
        }
    }
}
