namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder22 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScreenShareRequestTable", "ConnectionID", "dbo.ConnectionTable");
            DropForeignKey("dbo.ScreenShareRequestTable", "RequesterID", "dbo.UserTable");
            AddForeignKey("dbo.ScreenShareRequestTable", "ConnectionID", "dbo.ConnectionTable", "ID");
            AddForeignKey("dbo.ScreenShareRequestTable", "RequesterID", "dbo.UserTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenShareRequestTable", "RequesterID", "dbo.UserTable");
            DropForeignKey("dbo.ScreenShareRequestTable", "ConnectionID", "dbo.ConnectionTable");
            AddForeignKey("dbo.ScreenShareRequestTable", "RequesterID", "dbo.UserTable", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ScreenShareRequestTable", "ConnectionID", "dbo.ConnectionTable", "ID", cascadeDelete: true);
        }
    }
}
