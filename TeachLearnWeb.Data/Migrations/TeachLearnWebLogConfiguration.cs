namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class TeachLearnWebLogConfiguration : DbMigrationsConfiguration<TeachLearnWeb.Data.DbContextFolder.TeachLearnWebLogDbContext>
    {
        public TeachLearnWebLogConfiguration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(TeachLearnWeb.Data.DbContextFolder.TeachLearnWebLogDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
