namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PostTable", "UserID", "dbo.UserTable");
            DropIndex("dbo.PostTable", new[] { "UserID" });
            DropIndex("dbo.PostTable", new[] { "SentFeeds_ID" });
            DropIndex("dbo.PostTable", new[] { "FavouriteFeeds_ID" });
            RenameColumn(table: "dbo.PostTable", name: "FavouriteFeeds_ID", newName: "FavouriteFeedID");
            RenameColumn(table: "dbo.PostTable", name: "SentFeeds_ID", newName: "SentFeedID");
            AddColumn("dbo.PostTable", "PostDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PostTable", "SentFeedID", c => c.Int(nullable: false));
            AlterColumn("dbo.PostTable", "FavouriteFeedID", c => c.Int(nullable: false));
            CreateIndex("dbo.PostTable", "SentFeedID");
            CreateIndex("dbo.PostTable", "FavouriteFeedID");
            DropColumn("dbo.PostTable", "UserID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PostTable", "UserID", c => c.Int(nullable: false));
            DropIndex("dbo.PostTable", new[] { "FavouriteFeedID" });
            DropIndex("dbo.PostTable", new[] { "SentFeedID" });
            AlterColumn("dbo.PostTable", "FavouriteFeedID", c => c.Int());
            AlterColumn("dbo.PostTable", "SentFeedID", c => c.Int());
            DropColumn("dbo.PostTable", "PostDate");
            RenameColumn(table: "dbo.PostTable", name: "SentFeedID", newName: "SentFeeds_ID");
            RenameColumn(table: "dbo.PostTable", name: "FavouriteFeedID", newName: "FavouriteFeeds_ID");
            CreateIndex("dbo.PostTable", "FavouriteFeeds_ID");
            CreateIndex("dbo.PostTable", "SentFeeds_ID");
            CreateIndex("dbo.PostTable", "UserID");
            AddForeignKey("dbo.PostTable", "UserID", "dbo.UserTable", "ID", cascadeDelete: true);
        }
    }
}
