namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConnectionTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ConnectionString = c.String(),
                        ConnectionName = c.String(),
                        ViewerIP = c.String(),
                        SharerIP = c.String(),
                        ViewerID = c.Int(nullable: false),
                        SharerID = c.Int(nullable: false),
                        IsConnectionEnded = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.SharerID)
                .ForeignKey("dbo.UserTable", t => t.ViewerID, cascadeDelete: true)
                .Index(t => t.ViewerID)
                .Index(t => t.SharerID);
            
            CreateTable(
                "dbo.UserTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(maxLength: 30),
                        Password = c.String(),
                        Salt = c.String(),
                        Email = c.String(),
                        IsVerified = c.Boolean(nullable: false),
                        ProfilePhoto = c.Binary(),
                        SentFeedsID = c.Int(),
                        FavouriteFeedsID = c.Int(),
                        IsAdmin = c.Boolean(nullable: false),
                        IsOnline = c.Boolean(nullable: false),
                        PendingStatus = c.String(),
                        SignalRConnectionID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Username, unique: true);
            
            CreateTable(
                "dbo.ConversationTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MessageTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MessageContent = c.String(),
                        SendTime = c.DateTime(nullable: false),
                        IsPinned = c.Boolean(nullable: false),
                        UserID = c.Int(nullable: false),
                        ConversationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.ConversationTable", t => t.ConversationID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.ConversationID);
            
            CreateTable(
                "dbo.FavouriteFeedsTable",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.PostTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        Attachment = c.Binary(),
                        UserID = c.Int(nullable: false),
                        PostTime = c.DateTime(nullable: false),
                        TopicID = c.Int(nullable: false),
                        SentFeeds_ID = c.Int(),
                        FavouriteFeeds_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.TopicTable", t => t.TopicID, cascadeDelete: true)
                .ForeignKey("dbo.SentFeedsTable", t => t.SentFeeds_ID)
                .ForeignKey("dbo.FavouriteFeedsTable", t => t.FavouriteFeeds_ID)
                .Index(t => t.UserID)
                .Index(t => t.TopicID)
                .Index(t => t.SentFeeds_ID)
                .Index(t => t.FavouriteFeeds_ID);
            
            CreateTable(
                "dbo.TopicTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SentFeedID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SentFeedsTable", t => t.SentFeedID, cascadeDelete: true)
                .Index(t => t.SentFeedID);
            
            CreateTable(
                "dbo.SentFeedsTable",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.ReviewTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        ReviewPoint = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.StudentTable",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Name = c.String(),
                        Surname = c.String(),
                        Birthday = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.ID, cascadeDelete: true)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.EducationTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UniversityID = c.Int(nullable: false),
                        FacultyID = c.Int(nullable: false),
                        DepartmentID = c.Int(nullable: false),
                        StudentID = c.Int(nullable: false),
                        PeriodID = c.Int(nullable: false),
                        Lesson_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DepartmentTable", t => t.DepartmentID)
                .ForeignKey("dbo.FacultyTable", t => t.FacultyID)
                .ForeignKey("dbo.UniversityTable", t => t.UniversityID)
                .ForeignKey("dbo.PeriodTable", t => t.PeriodID, cascadeDelete: true)
                .ForeignKey("dbo.LessonTable", t => t.Lesson_ID)
                .ForeignKey("dbo.StudentTable", t => t.StudentID, cascadeDelete: true)
                .Index(t => t.UniversityID)
                .Index(t => t.FacultyID)
                .Index(t => t.DepartmentID)
                .Index(t => t.StudentID)
                .Index(t => t.PeriodID)
                .Index(t => t.Lesson_ID);
            
            CreateTable(
                "dbo.DepartmentTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        FacultyID = c.Int(nullable: false),
                        UniversityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FacultyTable", t => t.FacultyID, cascadeDelete: true)
                .ForeignKey("dbo.UniversityTable", t => t.UniversityID)
                .Index(t => t.FacultyID)
                .Index(t => t.UniversityID);
            
            CreateTable(
                "dbo.FacultyTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UniversityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UniversityTable", t => t.UniversityID, cascadeDelete: true)
                .Index(t => t.UniversityID);
            
            CreateTable(
                "dbo.UniversityTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PeriodTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Semester = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LessonTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DepartmentID = c.Int(nullable: false),
                        PeriodID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DepartmentTable", t => t.DepartmentID, cascadeDelete: true)
                .ForeignKey("dbo.PeriodTable", t => t.PeriodID)
                .Index(t => t.DepartmentID)
                .Index(t => t.PeriodID);
            
            CreateTable(
                "dbo.ScoreTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LessonID = c.Int(nullable: false),
                        EducationID = c.Int(nullable: false),
                        Average = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EducationTable", t => t.EducationID, cascadeDelete: true)
                .ForeignKey("dbo.LessonTable", t => t.LessonID, cascadeDelete: true)
                .Index(t => t.LessonID)
                .Index(t => t.EducationID);
            
            CreateTable(
                "dbo.NoteTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        ResultPoint = c.Double(nullable: false),
                        Effect = c.Double(nullable: false),
                        ScoreID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ScoreTable", t => t.ScoreID, cascadeDelete: true)
                .Index(t => t.ScoreID);
            
            CreateTable(
                "dbo.ScreenShareRequestTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ConnectionID = c.Int(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ConnectionTable", t => t.ConnectionID, cascadeDelete: true)
                .Index(t => t.ConnectionID);
            
            CreateTable(
                "dbo.UserConversationTable",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        ConversationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserID, t.ConversationID })
                .ForeignKey("dbo.UserTable", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.ConversationTable", t => t.ConversationID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.ConversationID);
            
            CreateTable(
                "dbo.TopicFavouriteFeedsTable",
                c => new
                    {
                        TopicID = c.Int(nullable: false),
                        FavouriteFeedID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TopicID, t.FavouriteFeedID })
                .ForeignKey("dbo.TopicTable", t => t.TopicID, cascadeDelete: true)
                .ForeignKey("dbo.FavouriteFeedsTable", t => t.FavouriteFeedID, cascadeDelete: true)
                .Index(t => t.TopicID)
                .Index(t => t.FavouriteFeedID);
            
            CreateTable(
                "dbo.UserFriendTable",
                c => new
                    {
                        User_ID = c.Int(nullable: false),
                        User_ID1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_ID, t.User_ID1 })
                .ForeignKey("dbo.UserTable", t => t.User_ID)
                .ForeignKey("dbo.UserTable", t => t.User_ID1)
                .Index(t => t.User_ID)
                .Index(t => t.User_ID1);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenShareRequestTable", "ConnectionID", "dbo.ConnectionTable");
            DropForeignKey("dbo.ConnectionTable", "ViewerID", "dbo.UserTable");
            DropForeignKey("dbo.StudentTable", "ID", "dbo.UserTable");
            DropForeignKey("dbo.EducationTable", "StudentID", "dbo.StudentTable");
            DropForeignKey("dbo.NoteTable", "ScoreID", "dbo.ScoreTable");
            DropForeignKey("dbo.ScoreTable", "LessonID", "dbo.LessonTable");
            DropForeignKey("dbo.ScoreTable", "EducationID", "dbo.EducationTable");
            DropForeignKey("dbo.LessonTable", "PeriodID", "dbo.PeriodTable");
            DropForeignKey("dbo.EducationTable", "Lesson_ID", "dbo.LessonTable");
            DropForeignKey("dbo.LessonTable", "DepartmentID", "dbo.DepartmentTable");
            DropForeignKey("dbo.EducationTable", "PeriodID", "dbo.PeriodTable");
            DropForeignKey("dbo.FacultyTable", "UniversityID", "dbo.UniversityTable");
            DropForeignKey("dbo.EducationTable", "UniversityID", "dbo.UniversityTable");
            DropForeignKey("dbo.DepartmentTable", "UniversityID", "dbo.UniversityTable");
            DropForeignKey("dbo.EducationTable", "FacultyID", "dbo.FacultyTable");
            DropForeignKey("dbo.DepartmentTable", "FacultyID", "dbo.FacultyTable");
            DropForeignKey("dbo.EducationTable", "DepartmentID", "dbo.DepartmentTable");
            DropForeignKey("dbo.SentFeedsTable", "ID", "dbo.UserTable");
            DropForeignKey("dbo.ReviewTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.UserFriendTable", "User_ID1", "dbo.UserTable");
            DropForeignKey("dbo.UserFriendTable", "User_ID", "dbo.UserTable");
            DropForeignKey("dbo.FavouriteFeedsTable", "ID", "dbo.UserTable");
            DropForeignKey("dbo.PostTable", "FavouriteFeeds_ID", "dbo.FavouriteFeedsTable");
            DropForeignKey("dbo.TopicTable", "SentFeedID", "dbo.SentFeedsTable");
            DropForeignKey("dbo.PostTable", "SentFeeds_ID", "dbo.SentFeedsTable");
            DropForeignKey("dbo.PostTable", "TopicID", "dbo.TopicTable");
            DropForeignKey("dbo.TopicFavouriteFeedsTable", "FavouriteFeedID", "dbo.FavouriteFeedsTable");
            DropForeignKey("dbo.TopicFavouriteFeedsTable", "TopicID", "dbo.TopicTable");
            DropForeignKey("dbo.PostTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.UserConversationTable", "ConversationID", "dbo.ConversationTable");
            DropForeignKey("dbo.UserConversationTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.MessageTable", "ConversationID", "dbo.ConversationTable");
            DropForeignKey("dbo.MessageTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.ConnectionTable", "SharerID", "dbo.UserTable");
            DropIndex("dbo.UserFriendTable", new[] { "User_ID1" });
            DropIndex("dbo.UserFriendTable", new[] { "User_ID" });
            DropIndex("dbo.TopicFavouriteFeedsTable", new[] { "FavouriteFeedID" });
            DropIndex("dbo.TopicFavouriteFeedsTable", new[] { "TopicID" });
            DropIndex("dbo.UserConversationTable", new[] { "ConversationID" });
            DropIndex("dbo.UserConversationTable", new[] { "UserID" });
            DropIndex("dbo.ScreenShareRequestTable", new[] { "ConnectionID" });
            DropIndex("dbo.NoteTable", new[] { "ScoreID" });
            DropIndex("dbo.ScoreTable", new[] { "EducationID" });
            DropIndex("dbo.ScoreTable", new[] { "LessonID" });
            DropIndex("dbo.LessonTable", new[] { "PeriodID" });
            DropIndex("dbo.LessonTable", new[] { "DepartmentID" });
            DropIndex("dbo.FacultyTable", new[] { "UniversityID" });
            DropIndex("dbo.DepartmentTable", new[] { "UniversityID" });
            DropIndex("dbo.DepartmentTable", new[] { "FacultyID" });
            DropIndex("dbo.EducationTable", new[] { "Lesson_ID" });
            DropIndex("dbo.EducationTable", new[] { "PeriodID" });
            DropIndex("dbo.EducationTable", new[] { "StudentID" });
            DropIndex("dbo.EducationTable", new[] { "DepartmentID" });
            DropIndex("dbo.EducationTable", new[] { "FacultyID" });
            DropIndex("dbo.EducationTable", new[] { "UniversityID" });
            DropIndex("dbo.StudentTable", new[] { "ID" });
            DropIndex("dbo.ReviewTable", new[] { "UserID" });
            DropIndex("dbo.SentFeedsTable", new[] { "ID" });
            DropIndex("dbo.TopicTable", new[] { "SentFeedID" });
            DropIndex("dbo.PostTable", new[] { "FavouriteFeeds_ID" });
            DropIndex("dbo.PostTable", new[] { "SentFeeds_ID" });
            DropIndex("dbo.PostTable", new[] { "TopicID" });
            DropIndex("dbo.PostTable", new[] { "UserID" });
            DropIndex("dbo.FavouriteFeedsTable", new[] { "ID" });
            DropIndex("dbo.MessageTable", new[] { "ConversationID" });
            DropIndex("dbo.MessageTable", new[] { "UserID" });
            DropIndex("dbo.UserTable", new[] { "Username" });
            DropIndex("dbo.ConnectionTable", new[] { "SharerID" });
            DropIndex("dbo.ConnectionTable", new[] { "ViewerID" });
            DropTable("dbo.UserFriendTable");
            DropTable("dbo.TopicFavouriteFeedsTable");
            DropTable("dbo.UserConversationTable");
            DropTable("dbo.ScreenShareRequestTable");
            DropTable("dbo.NoteTable");
            DropTable("dbo.ScoreTable");
            DropTable("dbo.LessonTable");
            DropTable("dbo.PeriodTable");
            DropTable("dbo.UniversityTable");
            DropTable("dbo.FacultyTable");
            DropTable("dbo.DepartmentTable");
            DropTable("dbo.EducationTable");
            DropTable("dbo.StudentTable");
            DropTable("dbo.ReviewTable");
            DropTable("dbo.SentFeedsTable");
            DropTable("dbo.TopicTable");
            DropTable("dbo.PostTable");
            DropTable("dbo.FavouriteFeedsTable");
            DropTable("dbo.MessageTable");
            DropTable("dbo.ConversationTable");
            DropTable("dbo.UserTable");
            DropTable("dbo.ConnectionTable");
        }
    }
}
