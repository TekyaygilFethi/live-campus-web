namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostTable", "AttachmentUrl", c => c.String());
            DropColumn("dbo.PostTable", "Attachment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PostTable", "Attachment", c => c.Binary());
            DropColumn("dbo.PostTable", "AttachmentUrl");
        }
    }
}
