namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder25 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserTable", "IsLoginToDesktop", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserTable", "IsLoginToDesktop");
        }
    }
}
