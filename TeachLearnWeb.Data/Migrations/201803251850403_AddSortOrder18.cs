namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder18 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ConnectionTable", "User_ID", "dbo.UserTable");
            DropIndex("dbo.ConnectionTable", new[] { "User_ID" });
            DropColumn("dbo.ConnectionTable", "User_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ConnectionTable", "User_ID", c => c.Int());
            CreateIndex("dbo.ConnectionTable", "User_ID");
            AddForeignKey("dbo.ConnectionTable", "User_ID", "dbo.UserTable", "ID");
        }
    }
}
