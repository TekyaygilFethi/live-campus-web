namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder27 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenShareRequestTable", "RequesterRequestType", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenShareRequestTable", "RequestToRequestType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenShareRequestTable", "RequestToRequestType");
            DropColumn("dbo.ScreenShareRequestTable", "RequesterRequestType");
        }
    }
}
