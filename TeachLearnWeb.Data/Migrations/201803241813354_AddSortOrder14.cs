namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder14 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserTable", "CreationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserTable", "CreationDate");
        }
    }
}
