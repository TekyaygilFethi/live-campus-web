namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostTable", "ForumFacultyID", c => c.Int(nullable: false));
            AddColumn("dbo.PostTable", "ForumDepartmentID", c => c.Int(nullable: false));
            CreateIndex("dbo.PostTable", "ForumFacultyID");
            CreateIndex("dbo.PostTable", "ForumDepartmentID");
            AddForeignKey("dbo.PostTable", "ForumDepartmentID", "dbo.ForumDepartmentTable", "ID");
            AddForeignKey("dbo.PostTable", "ForumFacultyID", "dbo.ForumFacultyTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostTable", "ForumFacultyID", "dbo.ForumFacultyTable");
            DropForeignKey("dbo.PostTable", "ForumDepartmentID", "dbo.ForumDepartmentTable");
            DropIndex("dbo.PostTable", new[] { "ForumDepartmentID" });
            DropIndex("dbo.PostTable", new[] { "ForumFacultyID" });
            DropColumn("dbo.PostTable", "ForumDepartmentID");
            DropColumn("dbo.PostTable", "ForumFacultyID");
        }
    }
}
