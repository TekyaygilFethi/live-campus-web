namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder17 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReviewTable", "Vote", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReviewTable", "Vote");
        }
    }
}
