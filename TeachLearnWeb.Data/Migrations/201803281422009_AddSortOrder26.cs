namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder26 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ScreenShareRequestTable", new[] { "RequestedConnectionID" });
            DropColumn("dbo.ScreenShareRequestTable", "NewConnectionID");
            RenameColumn(table: "dbo.ScreenShareRequestTable", name: "RequestedConnectionID", newName: "NewConnectionID");
            AddColumn("dbo.ScreenShareRequestTable", "RequestToID", c => c.Int(nullable: false));
            CreateIndex("dbo.ScreenShareRequestTable", "RequestToID");
            AddForeignKey("dbo.ScreenShareRequestTable", "RequestToID", "dbo.UserTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenShareRequestTable", "RequestToID", "dbo.UserTable");
            DropIndex("dbo.ScreenShareRequestTable", new[] { "RequestToID" });
            DropColumn("dbo.ScreenShareRequestTable", "RequestToID");
            RenameColumn(table: "dbo.ScreenShareRequestTable", name: "NewConnectionID", newName: "RequestedConnectionID");
            AddColumn("dbo.ScreenShareRequestTable", "NewConnectionID", c => c.Int());
            CreateIndex("dbo.ScreenShareRequestTable", "RequestedConnectionID");
        }
    }
}
