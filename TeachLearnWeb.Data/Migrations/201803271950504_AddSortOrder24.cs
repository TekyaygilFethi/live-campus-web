namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder24 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ScreenShareRequestTable", new[] { "ConnectionID" });
            RenameColumn(table: "dbo.ScreenShareRequestTable", name: "ConnectionID", newName: "RequestedConnectionID");
            AddColumn("dbo.ScreenShareRequestTable", "NewConnectionID", c => c.Int());
            AlterColumn("dbo.ScreenShareRequestTable", "RequestedConnectionID", c => c.Int());
            CreateIndex("dbo.ScreenShareRequestTable", "RequestedConnectionID");
            CreateIndex("dbo.ScreenShareRequestTable", "NewConnectionID");
            AddForeignKey("dbo.ScreenShareRequestTable", "NewConnectionID", "dbo.ConnectionTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenShareRequestTable", "NewConnectionID", "dbo.ConnectionTable");
            DropIndex("dbo.ScreenShareRequestTable", new[] { "NewConnectionID" });
            DropIndex("dbo.ScreenShareRequestTable", new[] { "RequestedConnectionID" });
            AlterColumn("dbo.ScreenShareRequestTable", "RequestedConnectionID", c => c.Int(nullable: false));
            DropColumn("dbo.ScreenShareRequestTable", "NewConnectionID");
            RenameColumn(table: "dbo.ScreenShareRequestTable", name: "RequestedConnectionID", newName: "ConnectionID");
            CreateIndex("dbo.ScreenShareRequestTable", "ConnectionID");
        }
    }
}
