namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class TeachLearnWebConfiguration : DbMigrationsConfiguration<TeachLearnWeb.Data.DbContextFolder.TeachLearnWebDbContext>
    {
        public TeachLearnWebConfiguration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DbContextFolder.TeachLearnWebDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
