namespace TeachLearnWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TopicTable", "ForumFacultyID", c => c.Int(nullable: false));
            AddColumn("dbo.TopicTable", "ForumDepartmentID", c => c.Int(nullable: false));
            CreateIndex("dbo.TopicTable", "ForumFacultyID");
            CreateIndex("dbo.TopicTable", "ForumDepartmentID");
            AddForeignKey("dbo.TopicTable", "ForumDepartmentID", "dbo.ForumDepartmentTable", "ID");
            AddForeignKey("dbo.TopicTable", "ForumFacultyID", "dbo.ForumFacultyTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TopicTable", "ForumFacultyID", "dbo.ForumFacultyTable");
            DropForeignKey("dbo.TopicTable", "ForumDepartmentID", "dbo.ForumDepartmentTable");
            DropIndex("dbo.TopicTable", new[] { "ForumDepartmentID" });
            DropIndex("dbo.TopicTable", new[] { "ForumFacultyID" });
            DropColumn("dbo.TopicTable", "ForumDepartmentID");
            DropColumn("dbo.TopicTable", "ForumFacultyID");
        }
    }
}
