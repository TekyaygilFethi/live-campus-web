﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnWeb.Security
{
    public static class HashSaltClass
    {
        static Repository<User> _userRepository = new Repository<User>(new Data.DbContextFolder.TeachLearnWebDbContext());

        public static string GenerateSalt()
        {
            return Crypto.GenerateSalt();
        }

        public static string GetHashSaltPassword(string _password, string _salt)
        {
            var _passwordLast = _password + _salt;

            return Crypto.HashPassword(_passwordLast);
        }

        public static async Task<bool> CheckCreedientals (string _usernamemail, string _password)
        {
            var _usernamedUser = await _userRepository.SingleGetByAsync(b => b.Username == _usernamemail);
            var _emailedUser = await _userRepository.SingleGetByAsync(x => x.Email == _usernamemail);

            User _user = null;
            if (_usernamedUser != null)
            {
                _user = _usernamedUser;
            }
            if (_emailedUser != null)
            {
                _user = _emailedUser;
            }
            if (_user != null)
            {
                var _userEnteredPassword = _password + _user.Salt;

                bool IsMatch = Crypto.VerifyHashedPassword(_user.Password, _userEnteredPassword);

                if (IsMatch)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}