﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachLearnWeb.Data;
using TeachLearnWeb.Business;
using TeachLearnWeb.Data.UniversityModel;
using TeachLearnWeb.Data.DbContextFolder;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using System.Threading;

namespace TeachLearnWeb.DataTransfer
{
    public class Program
    {
        static TeachLearnWebDbContext _liveCampusContext = new TeachLearnWebDbContext();
        static UnitOfWork _uow = new UnitOfWork(_liveCampusContext);

        static void Main(string[] args)
        {
            //UniversityExtension().GetAwaiter().GetResult();

            //Console.WriteLine("Period Metoduna Geçildiiiii!!!!!!!!!!!!!!");
            //Thread.Sleep(3000);

            //AddPeriods().GetAwaiter().GetResult();

            //Console.WriteLine("Friend Metoduna Geçildiiiii!!!!!!!!!!!!!!");

            //Thread.Sleep(3000);
            //AddFriendByManually();

            AddForumElements();

            Console.WriteLine("Done!");
            Console.ReadLine();


        }


        static async Task UniversityExtension()
        {


            IRepository<University> _universityRepository = _uow.GetRepository<University>();
            IRepository<Faculty> _facultyRepository = _uow.GetRepository<Faculty>();
            IRepository<Department> _departmentRepository = _uow.GetRepository<Department>();

            using (UniversitiesInTurkeyEntities _entities = new UniversitiesInTurkeyEntities())
            {
                foreach (var university in _entities.yok_universiteler.ToList())
                {
                    University _newUniversity = new University();
                    _newUniversity.ID = university.id;
                    _newUniversity.Name = university.name;

                    Console.WriteLine("UNIVERSITY:" + _newUniversity.Name + "\n-------------------------------");

                    foreach (var faculty in _entities.yok_fakulteler.Where(w => w.universiteId == university.id).ToList())
                    {
                        Faculty _newFaculty = new Faculty();
                        _newFaculty.ID = faculty.id;
                        _newFaculty.Name = faculty.name;
                        _newFaculty.University = _newUniversity;
                        _newUniversity.Faculties.Add(_newFaculty);
                        _newFaculty.UniversityID = _newUniversity.ID;

                        Console.WriteLine("FACULTY:" + _newFaculty.Name + "\n-------------------------------");
                        foreach (var department in _entities.yok_bolumler.Where(w => w.fakulteId == faculty.id.ToString()).ToList())
                        {
                            Department _newDepartment = new Department();
                            _newDepartment.ID = department.id;
                            _newDepartment.Name = department.name;
                            _newDepartment.Faculty = _newFaculty;
                            _newDepartment.University = _newUniversity;
                            _newDepartment.FacultyID = _newFaculty.ID;
                            _newDepartment.UniversityID = _newUniversity.ID;

                            _newFaculty.Departments.Add(_newDepartment);
                            _newUniversity.Departments.Add(_newDepartment);

                            Console.WriteLine("DEPARTMENT:" + _newDepartment.Name + "\n-------------------------------");

                            await _departmentRepository.AddAsync(_newDepartment);
                        }
                        await _facultyRepository.AddAsync(_newFaculty);
                    }
                    await _universityRepository.AddAsync(_newUniversity);
                }
                await _uow.SaveChanges();
            }


        }

        static async Task AddPeriods()
        {


            IRepository<Period> _periodRepository = _uow.GetRepository<Period>();

            for (int year = 1; year <= 6; year++)
            {
                for (int semester = 1; semester <= 2; semester++)
                {
                    Period _period = new Period();
                    _period.Year = year;
                    _period.Semester = semester;

                    await _periodRepository.AddAsync(_period);
                }
            }
            await _uow.SaveChanges();
        }

        static void AddFriendByManually()
        {
            using (var context = _liveCampusContext)
            {
                User Admin2 = context.Users.SingleOrDefault(w => w.Username == "Admin2");
                User Admin = context.Users.SingleOrDefault(w => w.Username == "Admin");
                User AdminVader = context.Users.SingleOrDefault(w => w.Username == "AdminVader");

                Admin.Friends.Add(Admin2);
                Admin.Friends.Add(AdminVader);

                Admin2.Friends.Add(Admin);
                Admin2.Friends.Add(AdminVader);

                AdminVader.Friends.Add(Admin2);
                AdminVader.Friends.Add(Admin);

                context.SaveChanges();
            }
        }

        static void AddForumElements()
        {
            using (var context = _liveCampusContext)
            {
                //    List<string> tempFacultyList = new List<string>();
                //    List<string> tempDepartmentList = new List<string>();

                //    foreach(var faculty in context.Faculties.ToList())
                //    {
                //        string facName = faculty.Name;

                //        if(!tempFacultyList.Contains(facName))
                //        {
                //            tempFacultyList.Add(facName);
                //        }

                //        foreach(var department in faculty.Departments)
                //        {
                //            string depName = department.Name.Contains('(') ? department.Name.Substring(0, department.Name.IndexOf('(')) : department.Name;//department.Name.Substring(0, department.Name.IndexOf('('));

                //            if (!tempDepartmentList.Contains(depName))
                //            {
                //                tempDepartmentList.Add(depName);
                //            }
                //        }
                //    }





                #region Faculty Addition
                ForumFaculty f1 = new ForumFaculty();
                f1.Name = "Basic Lessons";

                ForumFaculty f2 = new ForumFaculty();
                f2.Name = "Engineering";

                ForumFaculty f3 = new ForumFaculty();
                f3.Name = "Medicine, Pharmacy and Dentist";

                ForumFaculty f4 = new ForumFaculty();
                f4.Name = "Agriculture";

                ForumFaculty f5 = new ForumFaculty();
                f5.Name = "Fine Arts";

                ForumFaculty f6 = new ForumFaculty();
                f6.Name = "Sport";

                ForumFaculty f7 = new ForumFaculty();
                f7.Name = "Management";

                ForumFaculty f8 = new ForumFaculty();
                f8.Name = "Law";

                ForumFaculty f9 = new ForumFaculty();
                f9.Name = "Foreign Languages";

                ForumFaculty f10 = new ForumFaculty();
                f10.Name = "Architecture";

                ForumFaculty f11 = new ForumFaculty();
                f11.Name = "Theology";

                ForumFaculty f12 = new ForumFaculty();
                f12.Name = "Earth Sciences";

                ForumFaculty f13 = new ForumFaculty();
                f13.Name = "Industrial Arts";

                ForumFaculty f14 = new ForumFaculty();
                f14.Name = "Civil Aviation";
                #endregion
                
                #region Department Addition
                ForumDepartment d1 = new ForumDepartment();
                d1.Name = "Physics";
                d1.ForumFaculty = f1;

                ForumDepartment d2 = new ForumDepartment();
                d2.Name = "Biology";
                d2.ForumFaculty = f1;

                ForumDepartment d3 = new ForumDepartment();
                d3.Name = "Chemistry";
                d3.ForumFaculty = f1;

                ForumDepartment d4 = new ForumDepartment();
                d4.Name = "Geology";
                d4.ForumFaculty = f1;

                ForumDepartment d5 = new ForumDepartment();
                d5.Name = "Mathematic";
                d5.ForumFaculty = f1;

                ForumDepartment d6 = new ForumDepartment();
                d6.Name = "Computer Engineering";
                d6.ForumFaculty = f2;

                ForumDepartment d7 = new ForumDepartment();
                d7.Name = "Civil Engineering";
                d7.ForumFaculty = f2;

                ForumDepartment d8 = new ForumDepartment();
                d8.Name = "Mechanical Engineering";
                d8.ForumFaculty = f2;


                ForumDepartment d9 = new ForumDepartment();
                d9.Name = "Electric and Electronical Engineering";
                d9.ForumFaculty = f2;




                ForumDepartment d10 = new ForumDepartment();
                d10.Name = "Medicine, Pharmacy and Dentist";
                d10.ForumFaculty = f3;

                ForumDepartment d11 = new ForumDepartment();
                d11.Name = "Agriculture";
                d11.ForumFaculty = f4;

                ForumDepartment d12 = new ForumDepartment();
                d12.Name = "Fine Arts";
                d12.ForumFaculty = f5;

                ForumDepartment d13 = new ForumDepartment();
                d13.Name = "Sport";
                d13.ForumFaculty = f6;

                ForumDepartment d14 = new ForumDepartment();
                d14.Name = "Management";
                d14.ForumFaculty = f7;

                ForumDepartment d15 = new ForumDepartment();
                d15.Name = "Law";
                d15.ForumFaculty = f8;

                ForumDepartment d16 = new ForumDepartment();
                d16.Name = "Foreign Languages";
                d16.ForumFaculty = f9;

                ForumDepartment d21 = new ForumDepartment();
                d21.Name = "Architecture";
                d21.ForumFaculty = f10;


                ForumDepartment d17 = new ForumDepartment();
                d17.Name = "Theology";
                d17.ForumFaculty = f11;



                ForumDepartment d18 = new ForumDepartment();
                d18.Name = "Earth Sciences";
                d18.ForumFaculty = f12;



                ForumDepartment d19 = new ForumDepartment();
                d19.Name = "Industrial Arts";
                d19.ForumFaculty = f13;



                ForumDepartment d20 = new ForumDepartment();
                d20.Name = "Civil Aviation";
                d20.ForumFaculty = f14;
                #endregion
                
                f1.ForumDepartments.Add(d1);
                f1.ForumDepartments.Add(d2);
                f1.ForumDepartments.Add(d3);
                f1.ForumDepartments.Add(d4);
                f1.ForumDepartments.Add(d5);
                f2.ForumDepartments.Add(d6);
                f2.ForumDepartments.Add(d7);
                f2.ForumDepartments.Add(d8);
                f2.ForumDepartments.Add(d9);
                f3.ForumDepartments.Add(d10);
                f4.ForumDepartments.Add(d11);
                f5.ForumDepartments.Add(d12);
                f6.ForumDepartments.Add(d13);
                f7.ForumDepartments.Add(d14);
                f8.ForumDepartments.Add(d15);
                f9.ForumDepartments.Add(d16);
                f10.ForumDepartments.Add(d21);
                f11.ForumDepartments.Add(d17);
                f12.ForumDepartments.Add(d18);
                f13.ForumDepartments.Add(d19);
                f14.ForumDepartments.Add(d20);

                context.ForumDepartments.Add(d1);
                context.ForumDepartments.Add(d2);
                context.ForumDepartments.Add(d3);
                context.ForumDepartments.Add(d4);
                context.ForumDepartments.Add(d5);
                context.ForumDepartments.Add(d6);
                context.ForumDepartments.Add(d7);
                context.ForumDepartments.Add(d8);
                context.ForumDepartments.Add(d9);
                context.ForumDepartments.Add(d10);
                context.ForumDepartments.Add(d11);
                context.ForumDepartments.Add(d12);
                context.ForumDepartments.Add(d13);
                context.ForumDepartments.Add(d14);
                context.ForumDepartments.Add(d15);
                context.ForumDepartments.Add(d16);
                context.ForumDepartments.Add(d17);
                context.ForumDepartments.Add(d18);
                context.ForumDepartments.Add(d19);
                context.ForumDepartments.Add(d20);
                context.ForumDepartments.Add(d21);

                context.ForumFaculties.Add(f1);
                context.ForumFaculties.Add(f2);
                context.ForumFaculties.Add(f3);
                context.ForumFaculties.Add(f4);
                context.ForumFaculties.Add(f5);
                context.ForumFaculties.Add(f6);
                context.ForumFaculties.Add(f7);
                context.ForumFaculties.Add(f8);
                context.ForumFaculties.Add(f9);
                context.ForumFaculties.Add(f10);
                context.ForumFaculties.Add(f11);
                context.ForumFaculties.Add(f12);
                context.ForumFaculties.Add(f13);
                context.ForumFaculties.Add(f14);


                context.SaveChanges();


            }
        }
    }
}
